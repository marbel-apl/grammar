package marbel.cognitiveSwiprolog;

import static org.junit.Assert.assertArrayEquals;

import org.jpl7.JPL;
import org.junit.Test;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveSwiprolog.CognitiveSwiProlog;
import marbel.krInterface.language.Term;
import marbel.swiprolog.SwiPrologInterface;
import marbel.swiprolog.language.PrologCompound;
import marbel.swiprolog.language.PrologTerm;
import marbel.swiprolog.language.impl.PrologImplFactory;

public class CognitiveSwiTranslatorTest {
	private final CognitiveKR translator = new CognitiveSwiProlog(new SwiPrologInterface());
	private final PrologCompound a = PrologImplFactory.getAtom("a", null);
	private final PrologCompound b = PrologImplFactory.getAtom("b", null);
	private final PrologCompound c = PrologImplFactory.getAtom("c", null);
	private final PrologTerm one = PrologImplFactory.getNumber(1.0, null);
	private final PrologCompound nil = PrologImplFactory.getAtom(JPL.LIST_NIL.name(), null);

	@Test
	public void testUnpackWeirdSimpleList() throws Exception {
		final PrologCompound term = PrologImplFactory.getCompound(JPL.LIST_PAIR, new Term[] { this.a, this.b }, null);
		assertArrayEquals(new Term[] { this.a, this.b }, this.translator.unpackTerm(term).toArray(new Term[0]));
	}

	@Test
	public void testUnpackSimpleList() throws Exception {
		final PrologCompound term = PrologImplFactory.getCompound(JPL.LIST_PAIR, new Term[] { this.a, this.nil }, null);
		assertArrayEquals(new Term[] { this.a }, this.translator.unpackTerm(term).toArray(new Term[0]));
	}

	@Test
	public void testUnpackbadList() throws Exception {
		final PrologCompound term = PrologImplFactory.getCompound(JPL.LIST_PAIR, new Term[] { this.a, this.b, this.c },
				null);
		assertArrayEquals(new Term[] { term }, this.translator.unpackTerm(term).toArray(new Term[0]));
	}

	@Test
	public void testUnpackNonList() throws Exception {
		final PrologCompound term = PrologImplFactory.getCompound("ds", new Term[] { this.a, this.b }, null);
		assertArrayEquals(new Term[] { term }, this.translator.unpackTerm(term).toArray(new Term[0]));
	}

	@Test
	public void testUnpackNumber() throws Exception {
		assertArrayEquals(new Term[] { this.one }, this.translator.unpackTerm(this.one).toArray(new Term[0]));
	}

	@Test
	public void testUnpackNumbers() throws Exception {
		final PrologCompound list1 = PrologImplFactory.getCompound(JPL.LIST_PAIR, new Term[] { this.one, this.nil },
				null);
		final PrologCompound list2 = PrologImplFactory.getCompound(JPL.LIST_PAIR, new Term[] { this.one, list1 }, null);
		assertArrayEquals(new Term[] { this.one, this.one }, this.translator.unpackTerm(list2).toArray(new Term[0]));
	}
}
