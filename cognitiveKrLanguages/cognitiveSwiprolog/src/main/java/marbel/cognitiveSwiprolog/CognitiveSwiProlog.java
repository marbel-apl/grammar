/**
 * Knowledge Representation Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.cognitiveSwiprolog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.math.NumberUtils;
import org.jpl7.JPL;

import eis.iilang.Action;
import eis.iilang.Function;
import eis.iilang.Identifier;
import eis.iilang.Numeral;
import eis.iilang.Parameter;
import eis.iilang.ParameterList;
import eis.iilang.Percept;
import eis.iilang.TruthValue;
import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKr.TranslationException;
import marbel.krInterface.KRInterface;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Expression;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Update;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.swiprolog.language.PrologCompound;
import marbel.swiprolog.language.PrologDBFormula;
import marbel.swiprolog.language.PrologExpression;
import marbel.swiprolog.language.PrologQuery;
import marbel.swiprolog.language.PrologTerm;
import marbel.swiprolog.language.PrologUpdate;
import marbel.swiprolog.language.PrologVar;
import marbel.swiprolog.language.impl.PrologImplFactory;
import marbel.swiprolog.parser.SourceInfoObject;
import marbel.swiprolog.validator.SemanticTools;

/**
 * Implementation of {@link CognitiveKR} for SWI Prolog.
 */
public final class CognitiveSwiProlog extends CognitiveKR {
	public CognitiveSwiProlog(final KRInterface kri) {
		super(kri);
	}

	@Override
	protected SourceInfo getSourceInfo(final File source) throws IOException {
		return new SourceInfoObject(source.getCanonicalPath(), 0, 0, 0, 0);
	}

	@Override
	public Set<String> getDefinedSignatures(final DatabaseFormula formula) throws ParserException {
		return SemanticTools.getDefinedSignatures(((PrologDBFormula) formula).getCompound(), formula.getSourceInfo());
	}

	@Override
	public Set<String> getDeclaredSignatures(final DatabaseFormula formula) throws ParserException {
		return SemanticTools.getDeclaredSignatures(((PrologDBFormula) formula).getCompound(), formula.getSourceInfo());
	}

	@Override
	public List<Var> getAllVariables(final Expression expression) throws ParserException {
		return getVars(expression);
	}

	private static List<Var> getVars(final Expression expression) {
		final List<Var> vars = new ArrayList<>();
		if (expression instanceof PrologVar) {
			final PrologVar var = (PrologVar) expression;
			if (!var.isAnonymous()) {
				vars.add(var);
			}
		} else if (expression instanceof PrologCompound) {
			final PrologCompound compound = (PrologCompound) expression;
			for (final Term argument : compound) {
				vars.addAll(getVars(argument));
			}
		} else if (expression instanceof PrologDBFormula) {
			vars.addAll(getVars(((PrologDBFormula) expression).getCompound()));
		} else if (expression instanceof PrologQuery) {
			vars.addAll(getVars(((PrologQuery) expression).getCompound()));
		} else if (expression instanceof PrologUpdate) {
			vars.addAll(getVars(((PrologUpdate) expression).toQuery()));
		}
		return vars;
	}

	@Override
	public Set<String> getUsedSignatures(final Expression expression) {
		return SemanticTools.getUsedSignatures((PrologExpression) expression);
	}

	private final static Term LIST_NIL = PrologImplFactory.getAtom(JPL.LIST_NIL.name(), null);

	@Override
	public Term convert(final Parameter parameter) throws TranslationException {
		if (parameter instanceof Identifier) {
			// do not do quoting of the term, that is only for printing.
			return PrologImplFactory.getAtom(((Identifier) parameter).getValue(), null);
		} else if (parameter instanceof Numeral) {
			// check if parameter that is passed is a float.
			// note that LONG numbers are converted to float
			final Number number = ((Numeral) parameter).getValue();
			if (number instanceof Double || number instanceof Float || number.longValue() < Integer.MIN_VALUE
					|| number.longValue() > Integer.MAX_VALUE) {
				return PrologImplFactory.getNumber(number.doubleValue(), null);
			} else {
				return PrologImplFactory.getNumber(number.longValue(), null);
			}
		} else if (parameter instanceof Function) {
			final Function f = (Function) parameter;
			final List<Term> terms = new ArrayList<>(f.getParameters().size());
			for (final Parameter p : f.getParameters()) {
				terms.add(convert(p));
			}
			return PrologImplFactory.getCompound(f.getName(), terms.toArray(new Term[terms.size()]), null);
		} else if (parameter instanceof ParameterList) {
			final ParameterList pl = (ParameterList) parameter;
			final List<Term> terms = new ArrayList<>(pl.size());
			for (final Parameter p : pl) {
				terms.add(convert(p));
			}
			return makeList(terms);
		} else if (parameter instanceof TruthValue) {
			return PrologImplFactory.getAtom(((TruthValue) parameter).getValue(), null);
		} else {
			throw new TranslationException("encountered EIS parameter '" + parameter + "' of unsupported type '"
					+ parameter.getClass().getCanonicalName() + "'.");
		}
	}

	@Override
	public Parameter convert(final Term term) throws TranslationException {
		if (term instanceof PrologCompound) {
			final PrologCompound compound = (PrologCompound) term;
			// Check whether we're dealing with a list or other operator.
			if (JPL.LIST_PAIR.equals(compound.getName()) && compound.getArity() == 2) {
				final List<Term> args = compound.getOperands(JPL.LIST_PAIR);
				final Iterator<Term> argsIterator = args.iterator();
				final List<Parameter> parameters = new ArrayList<>(args.size() - 1);
				while (argsIterator.hasNext()) {
					final Term arg = argsIterator.next();
					if (argsIterator.hasNext()) { // skip the last element (empty list)
						parameters.add(convert(arg));
					}
				}
				return new ParameterList(parameters);
			} else if (compound.getArity() == 0) {
				return new Identifier(compound.getName());
			} else {
				final List<Parameter> parameters = new ArrayList<>(compound.getArity());
				for (final Term arg : compound) {
					parameters.add(convert(arg));
				}
				return new Function(compound.getName(), parameters);
			}
		} else if (term instanceof PrologTerm && ((PrologTerm) term).isNumeric()) {
			return new Numeral(NumberUtils.createNumber(((PrologTerm) term).toString()));
		} else {
			throw new TranslationException("conversion of the term '" + term + "' of type '"
					+ term.getClass().getCanonicalName() + "' to an EIS parameter is not supported.");
		}
	}

	@Override
	public Action convert(final String name, final List<Term> terms) throws TranslationException {
		final List<Parameter> parameters = new ArrayList<>(terms.size());
		for (final Term term : terms) {
			parameters.add(convert(term));
		}
		return new Action(name, parameters);
	}

	@Override
	public DatabaseFormula convert(final Percept percept) throws TranslationException {
		// Get main operator name and parameters of the percept.
		final String name = percept.getName();
		final List<Parameter> parameters = percept.getParameters();
		// Construct a compound from the percept operator and parameters.
		final List<Term> terms = new ArrayList<>(parameters.size());
		for (final Parameter parameter : parameters) {
			terms.add(convert(parameter));
		}
		final PrologCompound compound = PrologImplFactory.getCompound(name, terms.toArray(new Term[terms.size()]),
				null);
		return PrologImplFactory.getDBFormula(compound);
	}

	@Override
	public Term makeList(final List<Term> terms) throws TranslationException {
		SourceInfo source = null;
		if (!terms.isEmpty()) {
			source = terms.get(0).getSourceInfo();
		}
		// Start with element in list, since innermost term of Prolog list is
		// the last term.
		Term list = LIST_NIL;
		for (int i = terms.size() - 1; i >= 0; i--) {
			list = PrologImplFactory.getCompound(JPL.LIST_PAIR, new Term[] { terms.get(i), list }, source);
		}
		return list;
	}

	@Override
	public List<Term> unpackTerm(final Term term) throws TranslationException {
		List<Term> unpacked = new ArrayList<>();
		if (LIST_NIL.equals(term)) {
			return unpacked;
		}
		if (term instanceof PrologCompound) {
			final PrologCompound comp = ((PrologCompound) term);
			if (JPL.LIST_PAIR.equals(comp.getName()) && comp.getArity() == 2) {
				unpacked = unpackTerm(comp.getArg(1));
				unpacked.add(0, comp.getArg(0));
				return unpacked;
			}
		}
		unpacked.add(term);
		return unpacked;
	}

	@Override
	public Update makeUpdate(final List<DatabaseFormula> formulas) throws TranslationException {
		final List<Term> terms = new ArrayList<>(formulas.size());
		for (final DatabaseFormula add : formulas) {
			terms.add(((PrologDBFormula) add).getCompound());
		}
		return PrologImplFactory.getUpdate(termsToConjunct(terms, null));
	}

	@Override
	public Update makeUpdate(final Term term) throws TranslationException {
		if (term instanceof PrologCompound) {
			return PrologImplFactory.getUpdate((PrologCompound) term);
		} else {
			throw new TranslationException("Cannot make update out of '" + term + "'");
		}
	}

	/**
	 * Returns a (possibly empty) conjunct containing the given terms.
	 *
	 * @param terms
	 * @param info  source info
	 * @return possibly empty conjunct containing the given terms
	 */
	public static PrologCompound termsToConjunct(final List<Term> terms, final SourceInfo info) {
		if (terms.isEmpty()) {
			return PrologImplFactory.getAtom("true", info);
		} else {
			// build up list last to first.
			PrologCompound list = (PrologCompound) terms.get(terms.size() - 1); // last
			for (int i = terms.size() - 2; i >= 0; i--) {
				list = PrologImplFactory.getCompound(",", new Term[] { terms.get(i), list }, info);
			}
			return list;
		}
	}

	@Override
	public boolean isSignature(final Term term) {
		return (term instanceof PrologCompound) && ((PrologCompound) term).isPredicateIndicator();
	}
}