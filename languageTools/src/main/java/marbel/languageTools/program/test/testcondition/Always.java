package marbel.languageTools.program.test.testcondition;

import marbel.languageTools.program.agent.MentalLiteral;

public class Always extends TestCondition {
	/**
	 * Constructs a new Always operator
	 *
	 * @param query to evaluate at each state change
	 */
	public Always(final MentalLiteral query, final String original) {
		super(query, original);
	}
}
