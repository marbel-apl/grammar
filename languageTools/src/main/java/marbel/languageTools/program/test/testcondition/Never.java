package marbel.languageTools.program.test.testcondition;

import marbel.languageTools.program.agent.MentalLiteral;

public class Never extends TestCondition {
	/**
	 * Constructs a new Never operator
	 *
	 * @param query to evaluate at each state change
	 */
	public Never(final MentalLiteral query, final String original) {
		super(query, original);
	}

	@Override
	public void setNestedCondition(final TestCondition nested) {
		throw new IllegalArgumentException("a never-condition cannot have a nested condition.");
	}
}
