package marbel.languageTools.program.test.testcondition;

import marbel.languageTools.program.agent.MentalLiteral;

public class Until extends TestCondition {
	/**
	 * Constructs a new Until operator
	 *
	 * @param query to evaluate
	 */
	public Until(final MentalLiteral query, final String original) {
		super(query, original);
	}

	@Override
	public void setNestedCondition(final TestCondition nested) {
		throw new IllegalArgumentException("an until-condition cannot have a nested condition.");
	}
}
