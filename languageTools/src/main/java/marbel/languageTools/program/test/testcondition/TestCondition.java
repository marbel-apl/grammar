package marbel.languageTools.program.test.testcondition;

import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.MentalLiteral;

/**
 * Abstract base for any test condition. Test conditions are evaluated in the
 * context of a running agent and need to provide an evaluator that can do so.
 */
public abstract class TestCondition {
	/**
	 * What the original test statement looks like
	 */
	private final String original;
	/**
	 * The mental state condition of the query
	 */
	protected final MentalLiteral query;
	/**
	 * An optional nested condition (... -> ...)
	 */
	protected TestCondition nested;

	/**
	 * @return the mental state condition of the query
	 */
	public MentalLiteral getQuery() {
		return this.query;
	}

	/**
	 * @return the nested condition (... -> ...) if it is present (null otherwise)
	 */
	public TestCondition getNestedCondition() {
		return this.nested;
	}

	/**
	 * @return true when a nested condition is present
	 */
	public boolean hasNestedCondition() {
		return (this.nested != null);
	}

	/**
	 * Creates a {@link TestCondition} using the mental state condition.
	 *
	 * @param query    A mental state condition.
	 * @param original A string representing the original test condition (what it
	 *                 looks like in the file)
	 */
	public TestCondition(final MentalLiteral query, final String original) {
		this.query = query;
		this.original = original;
	}

	/**
	 * Defines a nested condition (when ... -> ...)
	 *
	 * @param nested The nested TestCondition.
	 */
	public void setNestedCondition(final TestCondition nested) {
		this.nested = nested;
	}

	public SourceInfo getSourceInfo() {
		return (getQuery() == null) ? null : getQuery().getSourceInfo();
	}

	@Override
	public String toString() {
		return this.original;
	}

	@Override
	public int hashCode() {
		return this.original.hashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final TestCondition other = (TestCondition) obj;
		return this.original.equals(other.original);
	}
}
