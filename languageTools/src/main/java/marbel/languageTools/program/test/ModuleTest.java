/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import marbel.krInterface.language.Term;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.GoalParsedObject;
import marbel.languageTools.program.test.testcondition.TestCondition;

public class ModuleTest extends GoalParsedObject {
	/**
	 * Name of the module under test.
	 */
	private final String moduleName;
	/**
	 * Arguments of the module under test.
	 */
	private final List<Term> arguments;
	/**
	 * The (temporal) in-conditions
	 */
	private List<TestCondition> testConditions;

	/**
	 * @param moduleName name of the module
	 */
	public ModuleTest(final String moduleName, final List<Term> arguments, final SourceInfo info) {
		super(info);
		this.moduleName = moduleName;
		this.arguments = arguments;
	}

	public void setTestConditions(final List<TestCondition> testConditions) {
		this.testConditions = testConditions;
	}

	/**
	 * @return name of the module to test
	 */
	public String getModuleName() {
		return this.moduleName;
	}

	/**
	 * @return arguments of the module to test
	 */
	public List<Term> getModuleArguments() {
		return Collections.unmodifiableList(this.arguments);
	}

	/**
	 * @return signature of the module to test
	 */
	public String getModuleSignature() {
		return this.moduleName + "/" + this.arguments.size();
	}

	/**
	 * @return the test conditions (possibly empty).
	 */
	public List<TestCondition> getTestConditions() {
		return (this.testConditions == null) ? new ArrayList<>(0) : this.testConditions;
	}

	@Override
	public String toString() {
		return "ModuleTest [moduleName=" + this.moduleName + ", " + this.testConditions + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.moduleName, this.testConditions);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof ModuleTest)) {
			return false;
		}
		final ModuleTest other = (ModuleTest) obj;
		if (!Objects.equals(this.moduleName, other.moduleName)
				|| !Objects.equals(this.testConditions, other.testConditions)) {
			return false;
		}
		return true;
	}
}