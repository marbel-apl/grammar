package marbel.languageTools.program.test.testcondition;

import marbel.languageTools.program.agent.MentalLiteral;

public class Eventually extends TestCondition {
	/**
	 * Constructs a new Eventually operator
	 *
	 * @param query to evaluate at the end
	 */
	public Eventually(final MentalLiteral query, final String original) {
		super(query, original);
	}
}
