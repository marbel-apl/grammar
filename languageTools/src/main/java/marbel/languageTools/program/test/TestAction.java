package marbel.languageTools.program.test;

import java.util.Objects;

import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.test.testcondition.Until;

public class TestAction {
	private final ActionCombo action;
	private final Until condition;

	public TestAction(final ActionCombo action) {
		this.action = action;
		this.condition = null;
	}

	public TestAction(final ActionCombo action, final Until condition) {
		this.action = action;
		this.condition = condition;
	}

	public ActionCombo getAction() {
		return this.action;
	}

	public Until getCondition() {
		return this.condition;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("do").append(" ").append(this.action);
		return result.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.action, this.condition);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof TestAction)) {
			return false;
		}
		final TestAction other = (TestAction) obj;
		if (!Objects.equals(this.action, other.action) || !Objects.equals(this.condition, other.condition)) {
			return false;
		}
		return true;
	}
}
