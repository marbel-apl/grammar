/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.actions;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.analyzer.module.ModuleValidator;
import marbel.languageTools.parser.MOD2GParser;

/**
 * Sleeps the agent for the indicated amount of milliseconds.
 */
public class SleepAction extends NonMentalAction {
	/**
	 * Creates a {@link SleepAction}.
	 *
	 * @param parameter The parameter that determines how long we need to sleep
	 *                  (milliseconds).
	 */
	public SleepAction(final Term parameter, final SourceInfo info) {
		super(ModuleValidator.getTokenName(MOD2GParser.SLEEP), info);
		addParameter(parameter);
	}

	/**
	 * @return The term (milliseconds sleep)
	 */
	public Term getTerm() {
		return this.parameters.isEmpty() ? null : this.parameters.get(0);
	}

	@Override
	public SleepAction applySubst(final Substitution substitution) {
		return new SleepAction((getTerm() == null) ? null : getTerm().applySubst(substitution), this.info);
	}
}
