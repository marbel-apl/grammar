/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.rules;

import java.util.Objects;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.GoalParsedObject;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.actions.ActionCombo;

/**
 * A rule consists of a condition (body) and an action (head). The condition of
 * a rule is a {@link MentalStateCondition}. The action of a rule is an
 * {@link ActionCombo}. A rule is applicable if the condition of the rule AND
 * the precondition of the action hold. In that case, the action can be selected
 * for execution.
 */
public abstract class Rule extends GoalParsedObject {
	/**
	 * The condition of the rule.
	 */
	protected final MentalLiteral condition;
	/**
	 * The action of the rule.
	 */
	protected ActionCombo action;

	/**
	 * Creates a new {@link Rule}
	 *
	 * @param condition Determines when the rule is applicable.
	 * @param action    The action to perform if the rule is applicable.
	 */
	protected Rule(final MentalLiteral condition, final ActionCombo action, final SourceInfo info) {
		super(info);
		this.condition = condition;
		this.action = action;
	}

	/**
	 * Gets the condition (head) of this {@link Rule}.
	 *
	 * @return The condition of this {@link Rule} used for evaluating whether the
	 *         rule is applicable.
	 */
	public MentalLiteral getCondition() {
		return this.condition;
	}

	/**
	 * Returns the action of this rule.
	 *
	 * @return The {@link ActionCombo} that is performed if this {@link Rule} is
	 *         applied.
	 */
	public ActionCombo getAction() {
		return this.action;
	}

	/**
	 * Sets the {@link ActionCombo} for this {@link Rule}.
	 *
	 * @param action The action to be associated with this rule.
	 */
	public void setAction(final ActionCombo action) {
		this.action = action;
	}

	/**
	 * Applies a substitution to this rule.
	 *
	 * @param substitution A substitution.
	 * @return A rule where variables that are bound by the substitution have been
	 *         instantiated (or renamed).
	 */
	public abstract Rule applySubst(Substitution substitution);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = getClass().hashCode();
		result = prime * result + ((this.action == null) ? 0 : this.action.hashCode());
		return prime * result + ((this.condition == null) ? 0 : this.condition.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !getClass().equals(obj.getClass())) {
			return false;
		}
		final Rule other = (Rule) obj;
		if (!Objects.equals(this.action, other.action) || !Objects.equals(this.condition, other.condition)) {
			return false;
		}
		return true;
	}
}
