/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.program.Program;
import marbel.languageTools.program.UseClause.UseCase;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.rules.Rule;

/**
 * A module has a name and possibly parameters, module options, may define
 * macros, and should contain one or more rules.
 */
public class Module extends Program {
	/**
	 * Name of this module.
	 */
	private String name;
	/**
	 * Location of the module definition.
	 */
	private SourceInfo definition;
	/**
	 * The (possibly empty) list of parameters of this module.
	 *
	 * <p>
	 * For anonymous modules, the parameters are derived by the validator from the
	 * variables present in the rule that calls/triggers it.
	 * </p>
	 */
	private List<Var> parameters = new ArrayList<>(0);
	/**
	 * If not null: the default substitution (from the mas2g) to use
	 */
	private Substitution defaultSubstitution;

	// -------------------------------------------------------------
	// Module options.
	// -------------------------------------------------------------

	/**
	 * The exit condition that is used to decide when to exit the module. If no
	 * condition has been specified, then {@link #getExitCondition()} will return
	 * the default condition.
	 */
	private ExitCondition exitCondition;
	/**
	 * The rule evaluation order that is used to evaluate the module's rules. If no
	 * order has been set, then {@link #getRuleEvaluationOrder()} will return the
	 * default order.
	 */
	private RuleEvaluationOrder order;

	/**
	 * The rules specified in the module.
	 */
	private final List<Rule> rules = new ArrayList<>();
	/**
	 * True iff this module is anonymous
	 */
	private boolean anonymous;

	/**
	 * Creates an (empty) module.
	 *
	 * @param info A source info object.
	 */
	public Module(final FileRegistry registry, final SourceInfo info) {
		super(registry, info);
	}

	/**
	 * @return The name of this module.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name A name for this module.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	public SourceInfo getDefinition() {
		return this.definition;
	}

	public void setDefinition(final SourceInfo definition) {
		this.definition = definition;
		if (!isAnonymous()) {
			register(this);
		}
	}

	/**
	 * @return The list of formal parameters of this module.
	 */
	public List<Var> getParameters() {
		return Collections.unmodifiableList(this.parameters);
	}

	/**
	 * @return If not null: the default substitution to use
	 */
	public Substitution getDefaultSubstitution() {
		return this.defaultSubstitution;
	}

	/**
	 * @param parameters The parameter list of the module.
	 */
	public void setParameters(final List<Var> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @param defaultSub A default substitution to use for the module.
	 */
	public void setDefaultSubstitution(final Substitution substitution) {
		this.defaultSubstitution = substitution;
	}

	/**
	 * @return The signature of this {@link Module}, i.e., [name]/[nrOfPars]
	 */
	public String getSignature() {
		return this.name + "/" + this.parameters.size();
	}

	/**
	 * @return The exit condition of this module.
	 */
	public ExitCondition getExitCondition() {
		if (this.exitCondition == null) {
			// return default
			return ExitCondition.ALWAYS;
		} else {
			return this.exitCondition;
		}
	}

	/**
	 * Sets exit condition of this module, if option has not already been set.
	 *
	 * @param exitCondition A type of exit condition.
	 * @return {@code true} if the condition was set, {@code false} if condition had
	 *         already been set.
	 */
	public boolean setExitCondition(final ExitCondition exitCondition) {
		if (this.exitCondition == null) {
			this.exitCondition = exitCondition;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return The rule evaluation order of this module as specified in the program.
	 *         Note that inherited rule order is handled at runtime, because modules
	 *         can be reused by different parent modules that have different rule
	 *         orders.
	 */
	public RuleEvaluationOrder getRuleEvaluationOrder() {
		return this.order;
	}

	/**
	 * Sets exit condition of this module as specified in the program. Normally only
	 * called in the parser.
	 *
	 * @param order The rule evaluation order of this module as specified in the
	 *              program. Null if the module does not specify a rule order.
	 */
	public void setRuleEvaluationOrder(final RuleEvaluationOrder order) {
		this.order = order;
	}

	/**
	 * @return The decision rules of this module.
	 */
	public List<Rule> getRules() {
		return Collections.unmodifiableList(this.rules);
	}

	/**
	 * @param rule A decision rule of this module.
	 */
	public void addRule(final Rule rule) {
		register(rule.getCondition());
		for (final Action<?> action : rule.getAction()) {
			register(action);
		}
		this.rules.add(rule);
	}

	/**
	 * Mark this module as anonymous.
	 */
	public void setAnonymous() {
		this.anonymous = true;

	}

	/**
	 * A module is <i>anonymous</i> if it has no name. In that case, the module is
	 * used to represent a set of nested rules of a rule.
	 *
	 * @return {@code true} if the module has no name, {@code false} otherwise.
	 */
	public boolean isAnonymous() {
		return this.anonymous;
	}

	@SuppressWarnings("unchecked")
	public List<DatabaseFormula> getKR() {
		return (List<DatabaseFormula>) getItems(UseCase.KR_FILE);
	}

	@SuppressWarnings("unchecked")
	public List<Module> getUsedModules() {
		return (List<Module>) getItems(UseCase.MODULE);
	}

	/**
	 * Recursively searches for (implicitly) referenced modules. Terminates if no
	 * new modules are found.
	 *
	 * @return List of all (indirectly) referenced modules from initial list of
	 *         modules.
	 */
	public Set<Module> referencedModules(final Set<Module> found) {
		found.add(this);
		for (final Module module : getUsedModules()) {
			if (found.add(module)) {
				module.referencedModules(found);
			}
		}
		return found;
	}

	/**
	 * @return A string with the name and parameters of this module.
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		str.append(this.name);

		if (!this.parameters.isEmpty()) {
			str.append("(");
			final Iterator<Var> pars = this.parameters.iterator();
			while (pars.hasNext()) {
				str.append(pars.next());
				str.append(pars.hasNext() ? ", " : "");
			}
			str.append(")");
		}

		return str.toString();
	}

	// ----------------------------------------------------------------------------
	// Enum classes for exit conditions, and rule evaluation orders
	// ----------------------------------------------------------------------------

	/**
	 * The available options for the exit condition of a module.
	 *
	 * <p>
	 * This condition is evaluated each time that the rules of the module have been
	 * evaluated (according to the rule evaluation order associated with the
	 * module). That is, <i>after</i> the rules have been evaluated, it is checked
	 * whether the module should be exited.
	 * </p>
	 *
	 * <p>
	 * Note that modules can also be exited using the <code>exit-module</code>
	 * action.
	 * </p>
	 */
	public enum ExitCondition {
		/**
		 * The module should be exited once an evaluation step produced no executed
		 * actions.
		 */
		NOACTION,
		/**
		 * The module should always be exited after an evaluation step.<br>
		 * This is the default value.
		 */
		ALWAYS,
		/**
		 * The module never exits. This is the default for the main program. If the main
		 * program exits, the agent dies.
		 */
		NEVER;
	}

	/**
	 * The different orders in which rules can be evaluated.
	 */
	public enum RuleEvaluationOrder {
		/**
		 * The <i>first</i> applicable rule will be applied. If more than one
		 * instantiation of an if-then rule can be applied, only the <i>first applicable
		 * instantiation</i> will be applied.
		 */
		LINEAR,
		/**
		 * The <i>first</i> applicable rule will be applied. If more than one
		 * instantiation of an if-then rule can be applied, a <i>random applicable
		 * instantiation</i> will be applied.
		 */
		LINEARRANDOM,
		/**
		 * <i>All</i> applicable rules will be applied. If more than one instantiation
		 * of an if-then rule can be applied, only the <i>first</i> applicable
		 * instantiation will be applied.
		 */
		LINEARALL,
		/**
		 * <i>All</i> applicable rules will be applied. If more than one instantiation
		 * of an if-then rule can be applied, a <i>random applicable instantiation</i>
		 * will be applied.
		 */
		LINEARALLRANDOM,
		/**
		 * A <i>random</i> applicable rule will be applied. If the rule to be applied is
		 * an if-then rule and more than one instantiation of the rule can be applied, a
		 * <i>random applicable instantiation</i> will be applied.
		 */
		RANDOM,
		/**
		 * <i>All</i> applicable rules will be applied in <i>random order</i>. If more
		 * than one instantiation of an if-then rule can be applied, a <i>random
		 * applicable instantiation</i> will be applied.
		 */
		RANDOMALL;
	}
}