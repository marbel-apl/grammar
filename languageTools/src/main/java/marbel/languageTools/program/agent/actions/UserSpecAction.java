/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.actions;

import java.util.ArrayList;
import java.util.List;

import marbel.krInterface.KRInterface;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.parser.SourceInfo;

/**
 * A user-specified action of the form 'name(parameters)' with one or more
 * associated action specifications (i.e., precondition, postcondition pairs).
 * Parameters are optional.
 * <p>
 * A user-specified action should at least have one associated action
 * specification. In case an action has multiple action specifications the order
 * of the specifications in the program is taken into account: a specification
 * that occurs before another one is used whenever it is applicable.
 * </p>
 */
public class UserSpecAction extends NonMentalAction {
	/**
	 * Creates a {@link UserSpecAction} with name, parameter list, and sets flag
	 * whether action should be sent to external environment or not.
	 *
	 * @param name                  The name of the action.
	 * @param parameters            The action parameters.
	 * @param external              Parameter indicating whether action should be
	 *                              sent to external environment or not.
	 *                              {@code true} indicates that action should be
	 *                              sent to environment; {@code false} indicates
	 *                              that action should not be sent to environment.
	 * @param precondition
	 * @param positivePostCondition
	 * @param negativePostCondition
	 * @param info
	 * @param kr                    the {@link KRInterface}
	 */
	public UserSpecAction(final String name, final List<Term> parameters, final SourceInfo info) {
		super(name, info);
		for (final Term parameter : parameters) {
			addParameter(parameter);
		}
	}

	@Override
	public UserSpecAction applySubst(final Substitution substitution) {
		// Apply substitution to action parameters, pre- and post-condition.
		final List<Term> parameters = new ArrayList<>(this.parameters.size());
		for (final Term parameter : this.parameters) {
			parameters.add(parameter.applySubst(substitution));
		}
		return new UserSpecAction(getName(), parameters, getSourceInfo());
	}
}
