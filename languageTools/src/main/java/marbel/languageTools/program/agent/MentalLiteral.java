/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import marbel.krInterface.KRInterface;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.GoalParsedObject;

/**
 * A mental literal is an object of the form <selector>.bel(...) or
 * <selector>.goal(...), (or goal-a or a-goal). It can also be of the negated
 * form, so not(...).
 */
public class MentalLiteral extends GoalParsedObject {
	protected final Query query;
	protected final Set<String> signatures;
	protected final List<Var> free = new ArrayList<>();

	/**
	 * A mental literal is an atomic query on the mental state. Examples: bel(...),
	 * goal(...) or not(bel(...)).
	 *
	 * @param query A {@link KRInterface} query.
	 * @param info  Source info about this object.
	 */
	public MentalLiteral(final Query query, final Set<String> signatures, final SourceInfo info) {
		super(info);
		this.query = query;
		this.signatures = (signatures == null) ? new LinkedHashSet<>(0) : signatures;
		if (query != null) {
			this.free.addAll(this.query.getFreeVar());
		}
	}

	public MentalLiteral applySubst(final Substitution substitution) {
		return new MentalLiteral((this.query == null) ? null : this.query.applySubst(substitution), this.signatures,
				this.info);
	}

	public Query getFormula() {
		return this.query;
	}

	public Set<String> getUsedSignatures() {
		return Collections.unmodifiableSet(this.signatures);
	}

	public List<Var> getFreeVar() {
		return Collections.unmodifiableList(this.free);
	}

	public boolean isClosed() {
		return this.query.isClosed();
	}

	@Override
	public String toString() {
		return this.query.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		return prime * ((this.query == null) ? 0 : this.query.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final MentalLiteral other = (MentalLiteral) obj;
		if (!Objects.equals(this.query, other.query)) {
			return false;
		}
		return true;
	}
}
