package marbel.languageTools.program.agent.actions;

import marbel.krInterface.language.Term;
import marbel.krInterface.parser.SourceInfo;

/**
 * Actions like {@link ExitModuleAction}, {@link LogAction},
 * {@link ModuleCallAction}, etc, but not including {@link UserSpecAction}.
 *
 */

public abstract class NonMentalAction extends Action<Term> {
	public NonMentalAction(final String name, final SourceInfo info) {
		super(name, info);
	}
}
