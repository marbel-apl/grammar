/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import marbel.krInterface.KRInterface;
import marbel.krInterface.language.Expression;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.GoalParsedObject;

/**
 * An action that an agent can perform.
 * <p>
 * Instances of this primarily contain the action as called in the program; but
 * they can also contain pointers to specifications on how to execute the
 * action. For example, it can be putDown(X). When it is figured out this is a
 * user spec action, an additional link to the definition of the action can be
 * included in the object, eg. a ref to user spec action putDown(Y) with
 * pre/post conditions.
 *
 * <p>
 * There are two types of actions: so-called <i>built-in</i> (also called
 * reserved) actions and so-called <i>user-specified</i> actions. Adopting and
 * dropping a goal, inserting and deleting beliefs, and sending a message are
 * examples of built-in actions.
 * </p>
 * <p>
 * By default, whenever an agent is connected to an environment, a
 * user-specified action is sent to that environment for execution. A programmer
 * can indicate using the "@int" option (inserted directly after the action name
 * and parameters in the action specification) that an action should NOT be sent
 * to an environment.
 * </p>
 * <p>
 * Every action has a precondition and a postcondition. A precondition specifies
 * the conditions that need to hold to be able to perform the action
 * (successfully); a postcondition specifies the (expected) effects of the
 * action. Only user-specified actions may have <i>multiple</i> action
 * specifications, i.e., preconditions and corresponding postconditions.
 * </p>
 */
public abstract class Action<Parameter extends Expression> extends GoalParsedObject {
	/**
	 * The name of the action.
	 */
	protected final String name;
	/**
	 * The parameters of the action.
	 */
	protected final List<Parameter> parameters = new ArrayList<>();
	/**
	 * A cache of free variables in the parameters of the action.
	 */
	protected final List<Var> free = new ArrayList<>();

	/**
	 * Creates an action (without instantiating its parameters, if any).
	 *
	 * @param name The name of the action.
	 * @param info the source info of this parsed object.
	 */
	public Action(final String name, final SourceInfo info) {
		super(info);
		this.name = name;
	}

	/**
	 * Returns the name of this {@link Action}.
	 *
	 * @return The name of the action.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the parameters of this {@link Action}.
	 *
	 * @return The parameters of the action.
	 */
	public List<Parameter> getParameters() {
		return Collections.unmodifiableList(this.parameters);
	}

	/**
	 * Adds a parameter of the action.
	 *
	 * @param parameter The parameter to be added.
	 */
	public void addParameter(final Parameter parameter) {
		this.parameters.add(parameter);
		this.free.addAll(parameter.getFreeVar());
	}

	public String getSignature() {
		return this.name + "/" + this.parameters.size();
	}

	public List<Var> getFreeVar() {
		return Collections.unmodifiableList(this.free);
	}

	public boolean isClosed() {
		return this.free.isEmpty();
	}

	public abstract Action<?> applySubst(Substitution substitution);

	// TODO: move this functionality to KR interface...
	public Substitution mgu(final Action<?> other, final KRInterface kri) {
		if (other == null || !getSignature().equals(other.getSignature())
				|| this.parameters.size() != other.parameters.size()) {
			return null;
		} else if (this.parameters.isEmpty()) {
			return kri.getSubstitution(null);
		} else {
			// Get mgu for first parameter
			Substitution substitution = this.parameters.get(0).mgu(other.parameters.get(0));
			// Get mgu's for remaining parameters
			for (int i = 1; i < this.parameters.size() && substitution != null; i++) {
				final Substitution mgu = this.parameters.get(i).mgu(other.parameters.get(i));
				substitution = substitution.combine(mgu);
			}
			return substitution;
		}
	}

	/**
	 * Default implementation of string representation for an action.
	 */
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder(this.name);
		if (!this.parameters.isEmpty()) {
			str.append("(");
			for (int i = 0; i < this.parameters.size(); i++) {
				str.append(this.parameters.get(i));
				str.append(i < this.parameters.size() - 1 ? ", " : "");
			}
			str.append(")");
		}
		return str.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = getClass().hashCode();
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return prime * result + ((this.parameters == null) ? 0 : this.parameters.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final Action<?> other = (Action<?>) obj;
		if (!Objects.equals(this.name, other.name) || !Objects.equals(this.parameters, other.parameters)) {
			return false;
		}
		return true;
	}
}
