/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.actions;

import java.util.ArrayList;
import java.util.List;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.analyzer.module.ModuleValidator;
import marbel.languageTools.parser.MOD2GParser;

/**
 * Sleeps the agent for the indicated amount of milliseconds.
 */
public class SubscribeAction extends NonMentalAction {
	/**
	 * Creates a {@link SubscribeAction}.
	 *
	 * @param parameters The parameters that determine which identifiers to
	 *                   subscribe to.
	 */
	public SubscribeAction(final List<Term> parameters, final SourceInfo info) {
		super(ModuleValidator.getTokenName(MOD2GParser.SUBSCRIBE), info);
		for (final Term parameter : parameters) {
			addParameter(parameter);
		}
	}

	@Override
	public SubscribeAction applySubst(final Substitution substitution) {
		// Apply substitution to parameters.
		final List<Term> parameters = new ArrayList<>(this.parameters.size());
		for (final Term term : this.parameters) {
			parameters.add(term.applySubst(substitution));
		}
		// Create new action with instantiated parameters.
		return new SubscribeAction(parameters, this.info);
	}
}
