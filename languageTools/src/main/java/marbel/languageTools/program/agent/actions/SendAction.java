/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.actions;

import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.List;

import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.analyzer.module.ModuleValidator;
import marbel.languageTools.parser.MOD2GParser;

/**
 * Sends a message to one or more agents.
 * <p>
 * A {@link Selector} is used to indicate to which agent(s) the message should
 * be sent.
 * </p>
 * <p>
 * A message can have a {@link SentenceMood} and has content. The content of a
 * message is represented as a {@link DatabaseFormula} as it should be possible
 * to store a message in a database (i.e., the agent's mail box).
 * </p>
 */
public class SendAction extends NonMentalAction {
	/**
	 * Creates a {@link SendAction} that sends a message (content) to one or more
	 * agents.
	 */
	public SendAction(final List<Term> parameters, final SourceInfo info) {
		super(ModuleValidator.getTokenName(MOD2GParser.SEND), info);
		for (final Term parameter : parameters) {
			addParameter(parameter);
		}
	}

	/**
	 * @return The target
	 */
	public Term getTarget() {
		return (this.parameters.size() == 2) ? this.parameters.get(0) : null;
	}

	/**
	 * @return The term that is to be inserted.
	 */
	public Term getUpdate() {
		return (this.parameters.size() == 2) ? this.parameters.get(1) : null;
	}

	@Override
	public SendAction applySubst(final Substitution substitution) {
		// Apply substitution to parameters.
		final List<Term> parameters = new ArrayList<>(this.parameters.size());
		for (final Term term : this.parameters) {
			parameters.add(term.applySubst(substitution));
		}
		// Create new action with instantiated parameters.
		return new SendAction(parameters, this.info);
	}

	@Override
	public String toString() {
		return String.format("%1$s(%2$s)", this.name, (this.parameters == null) ? "" : this.parameters.toString());
	}
}
