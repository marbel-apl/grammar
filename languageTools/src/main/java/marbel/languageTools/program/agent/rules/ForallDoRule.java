/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.rules;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.actions.ActionCombo;

/**
 * <p>
 * A rule of the form:<br>
 * <code>&nbsp;&nbsp;&nbsp;&nbsp;forall CONDITION do RESULT.</code><br>
 * <br>
 * Where <code>CONDITION</code> is a {@link MentalStateCondition}, and
 * <code>RESULT</code> an {@link ActionCombo}.
 * </p>
 * <p>
 * When evaluated in a rule set, all possible substitutions that make the rule
 * valid may be executed. If the rule is selected for execution, <i>all</i>
 * valid substitutions must be executed.<br>
 * This type of rule can be located in any <code>program{}</code>-section of any
 * module in a GOAL agent.
 * </p>
 */
public class ForallDoRule extends Rule {
	/**
	 * Creates a new {@link ForallDoRule}.
	 *
	 * @param condition What should hold before the rule should be executed.
	 * @param action    The result of executing the rule.
	 */
	public ForallDoRule(final MentalLiteral condition, final ActionCombo action, final SourceInfo info) {
		super(condition, action, info);
	}

	@Override
	public ForallDoRule applySubst(final Substitution substitution) {
		return new ForallDoRule((this.condition == null) ? null : this.condition.applySubst(substitution),
				(this.action == null) ? null : this.action.applySubst(substitution), this.info);
	}

	@Override
	public String toString() {
		final String condition = (this.condition == null) ? "<missing condition>" : this.condition.toString();
		final String actions = (this.action == null) ? "<missing actions>" : this.action.toString();

		return "forall " + condition + " do " + actions;
	}

}
