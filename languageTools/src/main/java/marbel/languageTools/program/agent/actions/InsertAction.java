/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program.agent.actions;

import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Update;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.analyzer.module.ModuleValidator;
import marbel.languageTools.parser.MOD2GParser;

/**
 * Action that "inserts" an {@link Update} into a belief base of the agent.
 */
public class InsertAction extends MentalAction {
	/**
	 * Creates an {@link InsertAction}.
	 *
	 * @param update The {@link Update} to be inserted.
	 */
	public InsertAction(final Update update, final SourceInfo info) {
		super(ModuleValidator.getTokenName(MOD2GParser.INSERT), info);
		addParameter(update);
	}

	/**
	 * @return The update that is to be inserted.
	 */
	public Update getUpdate() {
		return this.parameters.isEmpty() ? null : this.parameters.get(0);
	}

	@Override
	public InsertAction applySubst(final Substitution substitution) {
		return new InsertAction((getUpdate() == null) ? null : getUpdate().applySubst(substitution), this.info);
	}
}
