package marbel.languageTools.program.agent;

public enum SubscriptionType {
	ADD, DELETE, REPLACE, UPDATE;
}
