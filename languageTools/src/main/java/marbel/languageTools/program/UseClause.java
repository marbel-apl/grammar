/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.program;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.utils.Extension;
import marbel.languageTools.utils.ReferenceResolver;

/**
 * Container for use clauses specified in the definition section of an agent in
 * a MAS file, in a module file, or an action specification file. Stores the
 * original reference, the resolved reference (i.e. one or more files), and the
 * use case type.
 */
public class UseClause extends GoalParsedObject {
	/**
	 * Reference of use clause.
	 */
	private final String reference;
	/**
	 * The raw string of potential parameters that can be passed to a module.
	 * Resolved to actual KR in the second pass of the MASValidator.
	 */
	private final String rawParameters;
	/**
	 * Use case of use clause.
	 */
	private UseCase useCase;

	/**
	 * Path to directory where source files which contains this use clause is
	 * located. This path is used to resolve references and locate the associated
	 * file(s).
	 */
	private final String relativePath;
	/**
	 * Resolved references, i.e. one more files the reference can be resolved.
	 */
	private List<URI> resolvedReferences = null;

	/**
	 * Creates a use clause.
	 *
	 * @param reference    A reference.
	 * @param rawParams    The raw string of potential parameters that can be passed
	 *                     to a module.
	 * @param useCase      A use case. Can be {@code null}; in that case, use case
	 *                     is assumed to be either an (auxiliary) module or an
	 *                     action specification.
	 * @param relativePath A relative path (path to directory where source file
	 *                     which contains the use clause is located).
	 */
	public UseClause(final String reference, final String rawParams, final UseCase useCase, final String relativePath,
			final SourceInfo info) {
		super(info);
		this.reference = reference;
		this.rawParameters = rawParams;
		this.useCase = useCase;
		this.relativePath = relativePath;
	}

	/**
	 * @return The reference of this use clause.
	 */
	public String getReference() {
		return this.reference;
	}

	/**
	 * @return The raw string of potential parameters that can be passed to a
	 *         module. Resolved to actual KR in the second pass of the MASValidator.
	 */
	public String getRawParameters() {
		return this.rawParameters;
	}

	/**
	 * Resolves reference in use clause to a list of files.
	 *
	 * @return The list of files if the reference could be resolved, {@code null}
	 *         otherwise.
	 */
	public List<File> getReferencedFiles() {
		final List<URI> references = getOrResolveReferences();
		final List<File> returned = new ArrayList<>();
		for (final URI reference : references) {
			if (isLocalFile(reference)) {
				try {
					final File uri = new File(reference);
					returned.add(new File(uri.getCanonicalPath()));
				} catch (final IOException e) {
					// FIXME
				}
			}
		}
		return returned;
	}

	/**
	 * @return The use case of this use clause.
	 */
	public UseCase getUseCase() {
		return this.useCase;
	}

	/**
	 * Resolves the reference, and, if successful, stores the file found.
	 *
	 * @return A list of files that matches the reference.
	 */
	public List<URI> getOrResolveReferences() {
		if (this.resolvedReferences != null) {
			return this.resolvedReferences;
		}
		final List<URI> files = new ArrayList<>();
		if (this.useCase != null) { // agent definitions
			for (final File f : ReferenceResolver.resolveReference(this.reference, Extension.MOD2G,
					this.relativePath)) {
				files.add(f.toURI());
			}
		}
		if (this.useCase == null) {
			for (final File f : ReferenceResolver.resolveReference(this.reference, Extension.MOD2G,
					this.relativePath)) {
				files.add(f.toURI());
				this.useCase = UseCase.MODULE;
			}
		}
		if (this.useCase == null) {
			for (final File f : ReferenceResolver.resolveReference(this.reference, Extension.MAS2G,
					this.relativePath)) {
				files.add(f.toURI());
				this.useCase = UseCase.MAS;
			}
		}
		if (this.useCase == null) {
			// Process reference for KR files.
			final List<File> kr = ReferenceResolver.resolveKRReference(this.reference, this.relativePath);
			for (final File krFile : kr) {
				files.add(krFile.toURI());
				this.useCase = UseCase.KR_FILE;
			}
			if (files.isEmpty()) {
				try {
					final URI uri = new URI(this.reference);
					if (uri.isAbsolute()) {
						files.add(uri);
					}
				} catch (URISyntaxException | NullPointerException e) {
					// Ignored; empty files list will be reported.
				}
			}
		}

		this.resolvedReferences = files;

		return files;
	}

	private static boolean isLocalFile(final URI uri) {
		final String scheme = uri.getScheme();
		return scheme != null && scheme.equalsIgnoreCase("file") && !hasHost(uri);
	}

	private static boolean hasHost(final URI uri) {
		final String host = uri.getHost();
		return host != null && !host.isEmpty();
	}

	@Override
	public String toString() {
		return "<Use clause: " + this.reference + ", " + this.useCase + ">\n";
	}

	// -------------------------------------------------------------
	// Use Case enum class
	// -------------------------------------------------------------

	/**
	 * Enum for use cases of references of use clauses.
	 */
	public enum UseCase {
		/**
		 * Use case for (dynamic) belief base of agent.
		 */
		KR_FILE("KR file"),
		/**
		 * Use case for module that initializes agent.
		 */
		INIT("initialisation module"),
		/**
		 * Use case for module that processes events received by agent.
		 */
		UPDATES("update module"),
		/**
		 * Use case for module for main decision making of agent.
		 */
		DECISIONS("decision module"),
		/**
		 * Use case for module when shutting down the agent.
		 */
		SHUTDOWN("shutdown module"),
		/**
		 * Use case for any auxiliary module.
		 */
		MODULE("module"),
		/**
		 * Use case for referencing a MAS in a test.
		 */
		MAS("multi-agent system");

		private String typeLabel;

		UseCase(final String typeLabel) {
			this.typeLabel = typeLabel;
		}

		@Override
		public String toString() {
			return this.typeLabel;
		}

		/**
		 * @param usecase A string representing a use case type.
		 * @return The use case type, or {@code null} if the string cannot be resolved
		 *         into a use case type.
		 */
		public static UseCase getUseCase(final String usecase) {
			try {
				return UseCase.valueOf(usecase.toUpperCase());
			} catch (final IllegalArgumentException e) {
				return null;
			}
		}
	}

	@Override
	public int hashCode() {
		getOrResolveReferences();
		final int prime = 31;
		final int result = 1;
		return prime * result + ((this.resolvedReferences == null) ? 0 : this.resolvedReferences.hashCode());
	}

	@Override
	public boolean equals(final Object obj) {
		getOrResolveReferences();
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof UseClause)) {
			return false;
		}
		final UseClause other = (UseClause) obj;
		// if (this.useCase != other.useCase) {
		// return false;
		// }
		return Objects.equals(this.resolvedReferences, other.resolvedReferences);
	}
}