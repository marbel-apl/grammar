package marbel.languageTools.program;

import marbel.krInterface.parser.ParsedObject;
import marbel.krInterface.parser.SourceInfo;

public abstract class GoalParsedObject implements ParsedObject {
	protected final SourceInfo info;

	protected GoalParsedObject(final SourceInfo info) {
		this.info = info;
	}

	@Override
	public final SourceInfo getSourceInfo() {
		return this.info;
	}

	@Override
	public abstract String toString();

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);
}
