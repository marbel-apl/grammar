package marbel.languageTools.errors;

import java.io.Serializable;
import java.util.Objects;

import marbel.krInterface.parser.SourceInfo;

public abstract class Message implements Serializable, Comparable<Message> {
	private static final long serialVersionUID = 6396169798566643352L;
	private SourceInfo source = null;

	public interface ValidatorMessageType {
		String toReadableString(String... args);
	}

	protected final ValidatorMessageType type;
	protected String[] args;

	public Message(final ValidatorMessageType type, final SourceInfo source, final String... args) {
		this.type = type;
		this.source = source;
		this.args = args;
	}

	public ValidatorMessageType getType() {
		return this.type;
	}

	public SourceInfo getSource() {
		return this.source;
	}

	public void setSource(final SourceInfo source) {
		this.source = source;
	}

	public String[] getArguments() {
		return this.args;
	}

	public void setArguments(final String... args) {
		this.args = args;
	}

	public String toShortString() {
		return this.type.toReadableString(this.args);
	}

	@Override
	public abstract String toString();

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || !(obj instanceof Message)) {
			return false;
		} else {
			final Message other = (Message) obj;
			if (!Objects.equals(this.source, other.getSource()) || !Objects.equals(this.type, other.getType())) {
				return false;
			}
			return true;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.source, this.type);
	}

	@Override
	public int compareTo(final Message other) {
		if (equals(other)) {
			return 0;
		} else if (other.getSource() == null) {
			return (this.source == null) ? 0 : -1;
		} else if (this.source == null) {
			return 1;
		} else {
			return before(this.source, other.getSource()) ? -1 : 1;
		}
	}

	/**
	 * @param info1 A source info object.
	 * @param info2 A source info object.
	 * @return {@code true} if source position of info1 object occurs before
	 *         position of info2 object.
	 */
	private static boolean before(final SourceInfo info1, final SourceInfo info2) {
		final boolean source = info1.getSource() != null && info2.getSource() != null
				&& (info1.getSource().compareTo(info2.getSource()) < 0);
		final boolean sourceEqual = info1.getSource() != null && info2.getSource() != null
				&& (info1.getSource().compareTo(info2.getSource()) == 0);
		final boolean lineNr = sourceEqual && (info1.getLineNumber() < info2.getLineNumber());
		final boolean lineNrEqual = (info1.getLineNumber() == info2.getLineNumber());
		final boolean position = sourceEqual && lineNrEqual
				&& (info1.getCharacterPosition() < info2.getCharacterPosition());
		return source || lineNr || position;
	}

}
