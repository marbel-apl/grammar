package marbel.languageTools.errors.module;

import java.util.MissingFormatArgumentException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import marbel.languageTools.errors.ValidatorWarning.ValidatorWarningType;

public enum ModuleWarning implements ValidatorWarningType {
	/**
	 * Each type of option should be used only once.
	 */
	DUPLICATE_OPTION,
	/**
	 * It should be possible to perform an action.
	 */
	EXITMODULE_CANNOT_REACH,
	/**
	 * Module parameter should be used.
	 */
	PARAMETER_NEVER_USED,
	/**
	 * The file is empty.
	 */
	EMPTY_FILE,
	/**
	 * The name of the module does not match the file name.
	 */
	MODULE_NAME_MISMATCH,
	/**
	 * A variable is unused.
	 */
	VARIABLE_UNUSED;

	private static final ResourceBundle BUNDLE = ResourceBundle
			.getBundle("marbel.languageTools.messages.ModuleWarningMessages");

	@Override
	public String toReadableString(final String... args) {
		try {
			return String.format(BUNDLE.getString(name()), (Object[]) args);
		} catch (final MissingResourceException e1) {
			if (args.length > 0) {
				return args[0];
			} else {
				return name();
			}
		} catch (final MissingFormatArgumentException e2) {
			return BUNDLE.getString(name());
		}
	}
}
