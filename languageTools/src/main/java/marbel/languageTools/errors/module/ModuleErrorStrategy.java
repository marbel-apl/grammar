package marbel.languageTools.errors.module;

import org.antlr.v4.runtime.Token;

import marbel.languageTools.errors.MyErrorStrategy;
import marbel.languageTools.errors.ParserError.SyntaxError;
import marbel.languageTools.parser.MOD2GParser;

public class ModuleErrorStrategy extends MyErrorStrategy {
	@Override
	public String prettyPrintToken(final Token t) {
		final String txt = prettyPrintToken(getSymbolType(t));
		switch (t.getType()) {
		case MOD2GParser.ID:
			return txt + " '" + t.getText() + "'";
		default:
			return txt;
		}
	}

	@Override
	public String prettyPrintToken(final int type) {
		switch (type) {
		case Token.EOF:
			return "end of file";
		case MOD2GParser.ID:
			return "an identifier";
		case MOD2GParser.PARLIST:
			return "a list of parameters";
		case MOD2GParser.StringLiteral:
			return "a double-quoted string";
		case MOD2GParser.SingleQuotedStringLiteral:
			return "a single-quoted string";
		default:
			// Do not improve, simply return token symbol as is
			return MOD2GParser.VOCABULARY.getDisplayName(type);
		}
	}

	@Override
	public String prettyPrintRuleContext(final int ruleIndex) {
		switch (ruleIndex) {
		case MOD2GParser.RULE_module:
			return "module";
		case MOD2GParser.RULE_useclause:
			return "a use clause";
		case MOD2GParser.RULE_option:
			return "a module option";
		case MOD2GParser.RULE_exitoption:
			return "an exit option";
		case MOD2GParser.RULE_orderoption:
			return "an order option";
		case MOD2GParser.RULE_rules:
			return "the rules of the module";
		case MOD2GParser.RULE_actioncombo:
			return "a combination of multiple actions";
		case MOD2GParser.RULE_action:
			return "an action";
		case MOD2GParser.RULE_builtin:
			return "a built-in action";
		default:
			return MOD2GParser.ruleNames[ruleIndex];
		}
	}

	@Override
	public SyntaxError getLexerErrorType(final Token token) {
		switch (token.getType()) {
		case MOD2GParser.StringLiteral:
			return SyntaxError.UNTERMINATEDSTRINGLITERAL;
		case MOD2GParser.SingleQuotedStringLiteral:
			return SyntaxError.UNTERMINATEDSINGLEQUOTEDSTRINGLITERAL;
		default:
			return null;
		}
	}
}
