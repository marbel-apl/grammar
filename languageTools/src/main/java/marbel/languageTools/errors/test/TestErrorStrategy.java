package marbel.languageTools.errors.test;

import org.antlr.v4.runtime.Token;

import marbel.languageTools.errors.MyErrorStrategy;
import marbel.languageTools.errors.ParserError.SyntaxError;
import marbel.languageTools.parser.TEST2GParser;

public class TestErrorStrategy extends MyErrorStrategy {
	@Override
	public String prettyPrintToken(final Token t) {
		final String txt = prettyPrintToken(getSymbolType(t));
		switch (t.getType()) {
		case TEST2GParser.ID:
			return txt + " '" + t.getText() + "'";
		case TEST2GParser.INT:
			return txt + " " + t.getText();
		default:
			return txt;
		}
	}

	@Override
	public String prettyPrintToken(final int type) {
		switch (type) {
		case Token.EOF:
			return "end of file";
		case TEST2GParser.ID:
			return "an identifier";
		case TEST2GParser.INT:
			return "a number";
		case TEST2GParser.PARLIST:
			return "a list of KR terms";
		case TEST2GParser.StringLiteral:
			return "a double-quoted string";
		case TEST2GParser.SingleQuotedStringLiteral:
			return "a single-quoted string";
		default:
			return TEST2GParser.VOCABULARY.getDisplayName(type);
		}
	}

	@Override
	public String prettyPrintRuleContext(final int ruleIndex) {
		switch (ruleIndex) {
		case TEST2GParser.RULE_useclause:
			return "a use clause";
		case TEST2GParser.RULE_action:
			return "a module call";
		case TEST2GParser.RULE_test:
			return "a test";
		case TEST2GParser.RULE_moduletest:
			return "a module test";
		case TEST2GParser.RULE_testcondition:
			return "a test condition";
		case TEST2GParser.RULE_temporaltest:
			return "a temporal test condition";
		case TEST2GParser.RULE_reacttest:
			return "a reactto test condition";
		case TEST2GParser.RULE_agenttest:
			return "an agent test";
		case TEST2GParser.RULE_testaction:
			return "an action to execute";
		case TEST2GParser.RULE_runcondition:
			return "a run condition for an action";
		default:
			return TEST2GParser.ruleNames[ruleIndex];
		}
	}

	@Override
	public SyntaxError getLexerErrorType(final Token token) {
		switch (token.getType()) {
		case TEST2GParser.StringLiteral:
			return SyntaxError.UNTERMINATEDSTRINGLITERAL;
		case TEST2GParser.SingleQuotedStringLiteral:
			return SyntaxError.UNTERMINATEDSINGLEQUOTEDSTRINGLITERAL;
		default:
			return null;
		}
	}
}
