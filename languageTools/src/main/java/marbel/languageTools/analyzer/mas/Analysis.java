package marbel.languageTools.analyzer.mas;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import marbel.krInterface.KRInterface;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Update;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.Validator;
import marbel.languageTools.analyzer.module.ModuleValidator;
import marbel.languageTools.analyzer.module.ModuleValidatorSecondPass;
import marbel.languageTools.program.Program;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.UserSpecAction;
import marbel.languageTools.program.mas.AgentDefinition;
import marbel.languageTools.program.mas.MASProgram;

/**
 * Collects analysis data from all components of the MAS. Needed for validation
 * and reporting.
 *
 */
public class Analysis {
	private final List<String> actionCalls = new ArrayList<>();
	private final List<Module> moduleDefinitions = new ArrayList<>();
	private final List<Module> unusedModuleDefinitions = new ArrayList<>();
	private final List<UserSpecAction> actionDefinitions = new ArrayList<>();
	private final List<DatabaseFormula> predicateDefinitions = new ArrayList<>();
	private final List<Query> predicateQueries = new ArrayList<>();
	private final List<Update> krUpdates = new ArrayList<>();
	private final List<DatabaseFormula> krUnused = new ArrayList<>();
	private final Map.Entry<File, String> override;
	/**
	 * The list of all validators that are created to recursively validating this
	 * MAS. This is used for global checks on the MAS level.
	 */
	private final List<Validator<?, ?, ?, ?>> subvalidators = new ArrayList<>();
	private final FileRegistry registry;
	private final MASProgram program;
	private final List<UseClause> krFiles = new ArrayList<>();
	private final List<UserSpecAction> unusedActionDefinitions = new ArrayList<>();
	private final List<Update> insertUpdates = new ArrayList<>();
	private final List<Update> deleteUpdates = new ArrayList<>();
	// list of all modules that is used by some agent as init module
	private final List<Module> initModules = new ArrayList<>();
	// list of all modules that is used by some agent as event module
	private final List<Module> eventModules = new ArrayList<>();

	/**
	 * Create a MAS analysis. After creation you should call {@link #run()} to do
	 * the analysis. FIXME just do it directly?
	 *
	 * @param registry   the {@link FileRegistry}
	 * @param masProgram the {@link MASProgram} to analyse
	 * @param override   can be null or an override of settings
	 */
	public Analysis(final FileRegistry registry, final MASProgram masProgram, final Map.Entry<File, String> override) {
		this.registry = registry;
		this.program = masProgram;
		this.override = override;
	}

	public List<String> getActionCalls() {
		return Collections.unmodifiableList(this.actionCalls);
	}

	public List<Module> getUnusedModuleDefinitions() {
		return Collections.unmodifiableList(this.unusedModuleDefinitions);
	}

	public List<Module> getModuleDefinitions() {
		return Collections.unmodifiableList(this.moduleDefinitions);
	}

	/**
	 * @return all unused action definitions
	 */
	public List<UserSpecAction> getUnusedActionDefinitions() {
		return Collections.unmodifiableList(this.unusedActionDefinitions);
	}

	/**
	 * @return all action definitions found
	 */
	public List<UserSpecAction> getActionDefinitions() {
		return Collections.unmodifiableList(this.actionDefinitions);
	}

	public List<DatabaseFormula> getPredicateDefinitions() {
		return Collections.unmodifiableList(this.predicateDefinitions);
	}

	/**
	 *
	 * @return all belief and goal queries from all modules
	 */
	public List<Query> getPredicateQueries() {
		return Collections.unmodifiableList(this.predicateQueries);
	}

	public List<Update> getKRUpdates() {
		return Collections.unmodifiableList(this.krUpdates);
	}

	/**
	 * @return the formulas that have been defined in the set of database formulas
	 *         but are not used (queried).
	 */
	public List<DatabaseFormula> getKRUnused() {
		return Collections.unmodifiableList(this.krUnused);
	}

	public List<Validator<?, ?, ?, ?>> getSubvalidators() {
		return this.subvalidators;
	}

	public FileRegistry getRegistry() {
		return this.registry;
	}

	public MASProgram getProgram() {
		return this.program;
	}

	/**
	 * @return KR-language files that are used as beliefs
	 */
	public List<UseClause> getKRFiles() {
		return Collections.unmodifiableList(this.krFiles);
	}

	/**
	 * @return null, or a map of override information
	 */
	public Map.Entry<File, String> getOverride() {
		return this.override;
	}

	/**
	 * @return all updates from insert actions done in the modules
	 */
	public List<Update> getInsertUpdates() {
		return Collections.unmodifiableList(this.insertUpdates);
	}

	/**
	 * @return all updates from delete actions done in the modules
	 */
	public List<Update> getDeleteUpdates() {
		return Collections.unmodifiableList(this.deleteUpdates);
	}

	/**
	 * @return all {@link Module}s that are used by some agent as init module.
	 */
	public List<Module> getInitModules() {
		return Collections.unmodifiableList(this.initModules);
	}

	/**
	 *
	 * @return all {@link Module}s that are used by some agent as event module.
	 */
	public List<Module> getEventModules() {
		return Collections.unmodifiableList(this.eventModules);
	}

	/**
	 * This runs the actual analysis of the MAS program. SIDE EFFECT: loads the
	 * {@link AgentDefinition}s with KRI
	 *
	 * @return an updated Analysis (this), or null if analysis failed.
	 */
	public void run() {
		final MASProgram mas = getProgram();
		if (mas == null) {
			return;
		}
		for (final String name : mas.getAgentNames()) {
			final AgentDefinition agent = mas.getAgentDefinition(name);
			if (agent == null) {
				continue;
			}
			if (agent.getInitUseClause() != null) {
				fullValidate(agent.getInitUseClause());
				final Module init = agent.getInitModule();
				if (init != null) {
					this.actionCalls.add(init.getSignature());
					this.initModules.add(init);
					// for (Rule initrule : init.getRules()) {
					// for (Action<?> initaction : initrule.getAction()) {
					// if (initaction instanceof SendAction) {
					// reportWarning(MASWarning.INIT_MESSAGING,
					// initaction.getSourceInfo());
					// }
					// }
					// }
				}
			}
			if (agent.getEventUseClause() != null) {
				fullValidate(agent.getEventUseClause());
				final Module event = agent.getEventModule();
				if (event != null) {
					this.actionCalls.add(event.getSignature());
					this.eventModules.add(event);
				}
			}
			if (agent.getMainUseClause() != null) {
				fullValidate(agent.getMainUseClause());
				final Module main = agent.getMainModule();
				if (main != null) {
					this.actionCalls.add(main.getSignature());
				}
			}
			if (agent.getShutdownUseClause() != null) {
				fullValidate(agent.getShutdownUseClause());
				final Module shutdown = agent.getShutdownModule();
				if (shutdown != null) {
					this.actionCalls.add(shutdown.getSignature());
				}
			}
		}
		// Set a KR interface for the MAS (based on the first one we can find)
		// CHECK can we do this first?
		KRInterface kri = null;
		for (final File file : getRegistry().getSourceFiles()) {
			kri = getRegistry().getProgram(file).getKRInterface();
			if (kri != null) {
				mas.setKRInterface(kri);
				break;
			}
		}
		if (kri == null) {
			return;
		}
		for (final String name : mas.getAgentNames()) {
			final AgentDefinition agent = mas.getAgentDefinition(name);
			agent.setKRInterface(kri);
		}
		if (getRegistry().hasAnyError()) {
			return;
		}

		// MAS-level validation of unused predicates, actions, and/or modules
		for (final Validator<?, ?, ?, ?> use : getSubvalidators()) {
			if (use instanceof ModuleValidator) {
				// Add to the set of defined modules.
				final ModuleValidator spec = (ModuleValidator) use;
				this.moduleDefinitions.add(spec.getProgram());
				// Fetch KR definitions and usages
				final ModuleValidatorSecondPass specPass = (ModuleValidatorSecondPass) use.getSecondPass();
				this.predicateDefinitions.addAll(specPass.getKRFormulas());
				this.predicateQueries.addAll(specPass.getKRQueries());

				this.insertUpdates.addAll(specPass.getInsertUpdates());
				this.deleteUpdates.addAll(specPass.getDeleteUpdates());

				// Fetch module/action calls
				this.actionCalls.addAll(specPass.getActionsUsed());
				this.krUpdates.addAll(specPass.getKRUpdates());
			}
		}

		// Process
		this.unusedModuleDefinitions.addAll(this.moduleDefinitions);
		for (final Module module : this.unusedModuleDefinitions
				.toArray(new Module[this.unusedModuleDefinitions.size()])) {
			if (this.actionCalls.contains(module.getSignature())) {
				this.unusedModuleDefinitions.remove(module);
			}
		}
		this.unusedActionDefinitions.addAll(this.actionDefinitions);
		for (final UserSpecAction action : this.unusedActionDefinitions
				.toArray(new UserSpecAction[this.unusedActionDefinitions.size()])) {
			if (this.actionCalls.contains(action.getSignature())) {
				this.unusedActionDefinitions.remove(action);
			}
		}
		final Set<String> had = new LinkedHashSet<>();
		for (final DatabaseFormula unused : kri.getUnused(this.predicateDefinitions, this.predicateQueries)) {
			if (had.add(unused.getSignature())) {
				this.krUnused.add(unused);
			}
		}
	}

	/**
	 * create a {@link Validator} for the {@link UseClause}. Call
	 * {@link Validator#validate(false)}. If that succeeds with a {@link Program},
	 * recursively validate all useclauses of this program. Then call
	 * {@link Validator#validate(true)} to do the second pass of validation.
	 *
	 * If the {@link Program} for this use is already in the registry, this returns
	 * immediately.
	 *
	 * As side-effects, this updates (1) the {@link #registry} (2) the
	 * {@link #subvalidators}
	 *
	 * @param use the {@link UseClause} to check.
	 */
	private void fullValidate(final UseClause use) {
		if (use == null || use.getUseCase() == null) {
			return;
		}
		for (final File file : use.getReferencedFiles()) {
			// Check for existing...
			final Program returned = this.registry.getProgram(file);
			if (returned != null) {
				continue;
			}
			// Parse if we need to (first pass).
			Validator<?, ?, ?, ?> validator = null;
			switch (use.getUseCase()) {
			case DECISIONS:
			case INIT:
			case UPDATES:
			case SHUTDOWN:
			case MODULE:
				try {
					validator = new ModuleValidator(file.getCanonicalPath(), this.registry);
				} catch (final IOException e) {
					e.printStackTrace();
				}
				break;
			case KR_FILE:
				this.krFiles.add(use);
				break;
			default:
				break;
			}
			if (validator != null) {
				getSubvalidators().add(validator);
				if (getOverride() != null && validator.getSource().equals(getOverride().getKey())) {
					validator.override(getOverride().getValue());
				}
				// Do a first pass on the current file, and recursively process any
				// use case we encountered in this file's first pass.
				validator.validate(false);
				if (validator.getProgram() != null) {
					for (final UseClause sub : validator.getProgram().getUseClauses()) {
						fullValidate(sub);
					}
				}
				// Execute the second pass (so after all dependencies have been
				// fully processed).
				validator.validate(true);
			}
		}
	}
}
