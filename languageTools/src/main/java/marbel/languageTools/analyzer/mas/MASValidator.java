/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.analyzer.mas;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.io.FilenameUtils;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKrFactory.CognitiveKRFactory;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.krInterface.KRInterface;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Substitution;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.InputStreamPosition;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.Validator;
import marbel.languageTools.analyzer.ValidatorSecondPass;
import marbel.languageTools.errors.ValidatorWarning;
import marbel.languageTools.errors.mas.MASError;
import marbel.languageTools.errors.mas.MASErrorStrategy;
import marbel.languageTools.errors.mas.MASWarning;
import marbel.languageTools.parser.MAS2GLexer;
import marbel.languageTools.parser.MAS2GParser;
import marbel.languageTools.parser.MAS2GParser.AgentContext;
import marbel.languageTools.parser.MAS2GParser.ChannelCaseContext;
import marbel.languageTools.parser.MAS2GParser.ChannelClauseContext;
import marbel.languageTools.parser.MAS2GParser.ConstantContext;
import marbel.languageTools.parser.MAS2GParser.ConstraintContext;
import marbel.languageTools.parser.MAS2GParser.EntityContext;
import marbel.languageTools.parser.MAS2GParser.EntitynameContext;
import marbel.languageTools.parser.MAS2GParser.EntitytypeContext;
import marbel.languageTools.parser.MAS2GParser.EnvironmentContext;
import marbel.languageTools.parser.MAS2GParser.InitExprContext;
import marbel.languageTools.parser.MAS2GParser.InitKeyValueContext;
import marbel.languageTools.parser.MAS2GParser.InstructionContext;
import marbel.languageTools.parser.MAS2GParser.LaunchRuleContext;
import marbel.languageTools.parser.MAS2GParser.ListContext;
import marbel.languageTools.parser.MAS2GParser.MasContext;
import marbel.languageTools.parser.MAS2GParser.MaxconstraintContext;
import marbel.languageTools.parser.MAS2GParser.NameconstraintContext;
import marbel.languageTools.parser.MAS2GParser.NrconstraintContext;
import marbel.languageTools.parser.MAS2GParser.PolicyContext;
import marbel.languageTools.parser.MAS2GParser.RefContext;
import marbel.languageTools.parser.MAS2GParser.StringContext;
import marbel.languageTools.parser.MAS2GParser.UseCaseContext;
import marbel.languageTools.parser.MAS2GParser.UseClauseContext;
import marbel.languageTools.parser.MAS2GParserVisitor;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.UseClause.UseCase;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.SubscriptionType;
import marbel.languageTools.program.agent.actions.UserSpecAction;
import marbel.languageTools.program.mas.AgentDefinition;
import marbel.languageTools.program.mas.Entity;
import marbel.languageTools.program.mas.LaunchInstruction;
import marbel.languageTools.program.mas.LaunchRule;
import marbel.languageTools.program.mas.MASProgram;
import marbel.languageTools.symbolTable.AgentSymbol;
import marbel.languageTools.symbolTable.SymbolTable;
import marbel.languageTools.utils.Extension;
import marbel.languageTools.utils.ReferenceResolver;

/**
 * Validates a MAS file and constructs a MAS program.
 */
public class MASValidator extends Validator<MAS2GLexer, MAS2GParser, MASErrorStrategy, MASProgram>
		implements MAS2GParserVisitor<Object> {
	private static final String STAR = "*";

	private MAS2GParser parser;

	private static MASErrorStrategy strategy = null;

	/**
	 * Symbol table with (possible) agent names.
	 */
	private final SymbolTable agentNames = new SymbolTable();

	/**
	 * Creates a MAS validator for file with given name.
	 *
	 * @param filename Name of a file.
	 */
	public MASValidator(final String filename, final FileRegistry registry) {
		super(filename, registry);
	}

	@Override
	protected MASErrorStrategy getTheErrorStrategy() {
		if (strategy == null) {
			strategy = new MASErrorStrategy();
		}
		return strategy;
	}

	/**
	 * @return Symbol table with agent file references.
	 */
	public SymbolTable getSymbolTable() {
		return this.agentNames;
	}

	@Override
	protected MAS2GLexer getNewLexer(final CharStream stream) {
		return new MAS2GLexer(stream);
	}

	@Override
	protected MAS2GParser getNewParser(final TokenStream stream) {
		this.parser = new MAS2GParser(stream);
		return this.parser;
	}

	@Override
	protected ParseTree startParser() {
		return this.parser.mas();
	}

	@Override
	protected MASProgram getNewProgram(final File masfile) throws IOException {
		return new MASProgram(this.registry, new InputStreamPosition(0, 0, 0, 0, masfile.getCanonicalPath()));
	}

	@Override
	public MASProgram getProgram() {
		return (MASProgram) super.getProgram();
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation calls {@link ParseTree#accept} on the specified
	 * tree.
	 * </p>
	 */
	@Override
	public Void visit(final ParseTree tree) {
		tree.accept(this);
		return null; // Java says must return something even when Void
	}

	@Override
	public Void visitMas(final MasContext ctx) {
		if (ctx.environment() != null) {
			visitEnvironment(ctx.environment());
		}

		for (final AgentContext agentCtx : ctx.agent()) {
			visitAgent(agentCtx);
		}

		// TODO VALIDATE: Check whether KR files used by different agents belong
		// to one and the same KRT

		if (ctx.policy() != null) {
			visitPolicy(ctx.policy());
		}

		return null; // Java says must return something even when Void
	}

	// -------------------------------------------------------------
	// ENVIRONMENT section
	// -------------------------------------------------------------

	@Override
	public Void visitEnvironment(final EnvironmentContext ctx) {
		/**
		 * Get the path to the environment interface file. Should be a jar file.
		 *
		 * The MAS file is used as an absolute reference to resolve relative references.
		 */
		File environmentfile = null;
		String filename = "";
		if (ctx.ref() != null) {
			filename = visitRef(ctx.ref()).getKey();
			final List<File> resolved = ReferenceResolver.resolveReference(filename, Extension.JAR,
					getPathRelativeToSourceFile(""));
			environmentfile = (resolved.size() == 1) ? resolved.get(0) : null;
		}
		if (environmentfile == null || !environmentfile.isFile()) {
			reportError(MASError.ENVIRONMENT_COULDNOT_FIND, ctx, filename);
		} else {
			getProgram().setEnvironmentfile(environmentfile);
		}

		/**
		 * Get list of key-value initialization parameters.
		 */
		for (final InitKeyValueContext pair : ctx.initKeyValue()) {
			visitInitKeyValue(pair);
		}

		return null; // Java says must return something even when Void
	}

	@Override
	public Map.Entry<String, String> visitRef(final RefContext ctx) {
		if (ctx == null) {
			return new AbstractMap.SimpleEntry<>("", null);
		} else if (ctx.string() != null) {
			final String fullPath = visitString(ctx.string());
			return new AbstractMap.SimpleEntry<>(fullPath, null);
		} else {
			String path = "";
			for (final TerminalNode component : ctx.ID()) {
				path = FilenameUtils.concat(path, component.getText());
			}
			final String rawParams = (ctx.PARLIST() == null) ? null
					: removeLeadTrailCharacters(ctx.PARLIST().getText());
			return new AbstractMap.SimpleEntry<>(path, rawParams);
		}
	}

	@Override
	public Void visitInitKeyValue(final InitKeyValueContext ctx) {
		boolean problem = (ctx.exception != null);

		// We can't check whether specified environment interface supports
		// parameter key
		// Just check whether it's not used more than once
		String key = null;
		if (ctx.ID() != null) {
			key = ctx.ID().getText();
			if (getProgram().getInitParameters().containsKey(key)) {
				problem = reportWarning(MASWarning.INIT_DUPLICATE_KEY, ctx.ID(), key);
			}
		}

		// Get parameter value
		final Object value = (ctx.initExpr() == null) ? null : visitInitExpr(ctx.initExpr());

		// If value equals null, we did not recognize a valid initialization
		// parameter
		if (value == null && ctx.initExpr() != null) {
			problem = reportError(MASError.INIT_UNRECOGNIZED_PARAMETER, ctx.initExpr(), ctx.initExpr().getText());
		}

		// Add key-value pair as initialization parameter to MAS program (only
		// if no problems were detected).
		if (!problem && (key != null) && (value != null)) {
			getProgram().addInitParameter(key, value);
		}

		return null; // Java says must return something even when Void
	}

	/**
	 * @return {@code null} if no valid parameter was recognized.
	 */
	@Override
	public Object visitInitExpr(final InitExprContext ctx) {
		if (ctx.constant() != null) {
			return visitConstant(ctx.constant());
		} else if (ctx.list() != null) {
			return visitList(ctx.list());
		} else {
			return null;
		}
	}

	@Override
	public Object visitConstant(final ConstantContext ctx) {
		if (ctx.ID() != null) {
			return ctx.ID().getText();
		} else if (ctx.FLOAT() != null) {
			return Double.parseDouble(ctx.FLOAT().getText());
		} else if (ctx.INT() != null) {
			return Integer.parseInt(ctx.INT().getText());
		} else if (ctx.string() != null) {
			return visitString(ctx.string());
		} else {
			// We did not recognize a valid initialization parameter.
			reportError(MASError.INIT_UNRECOGNIZED_PARAMETER, ctx, ctx.getText());
			return null;
		}
	}

	@Override
	public List<Object> visitList(final ListContext ctx) {
		boolean problem = false;

		final int nrOfPars = (ctx.initExpr() == null) ? 0 : ctx.initExpr().size();
		final List<Object> list = new ArrayList<>(nrOfPars);
		for (int i = 0; i < nrOfPars; i++) {
			final InitExprContext expr = ctx.initExpr(i);
			if (expr == null) {
				problem = true;
				continue;
			}
			list.add(visitInitExpr(expr));
		}

		if (problem) {
			return null;
		} else {
			return list;
		}
	}

	// -------------------------------------------------------------
	// AGENT DEFINITIONS
	// -------------------------------------------------------------

	@Override
	public Void visitAgent(final AgentContext ctx) {
		String agentName;
		if (ctx.ID() == null) {
			// Something bad went wrong, we cannot continue
			return null;
		} else {
			// Get agent name
			agentName = ctx.ID().getText();
		}

		// Get the use clauses and resolve references
		final AgentDefinition agentDf = new AgentDefinition(agentName, this.registry, getSourceInfo(ctx));
		for (final UseClauseContext useClause : ctx.useClause()) {
			final UseClause clause = visitUseClause(useClause);
			// Check if something bad went wrong while parsing
			if (clause == null) {
				continue;
			}

			final List<URI> files = clause.getOrResolveReferences();
			if (files.isEmpty()) {
				reportError(MASError.REFERENCE_COULDNOT_FIND, useClause, clause.getReference());
			}

			// Add use clause to agent definition
			// VALIDATE: Check for duplicate init, event, or main modules
			if (!agentDf.addUseClause(clause)) {
				reportError(MASError.USECASE_DUPLICATE, useClause, clause.getReference());
			}
		}

		// Construct agent symbol and add it to symbol table for later reference
		// (if key does not yet exist).
		// VALIDATE: Check whether agent has already been defined
		if (!this.agentNames.define(new AgentSymbol(agentName, agentDf, getSourceInfo(ctx.ID())))) {
			// Record problem to inform user that we do not overwrite first
			// agent definition
			// and simply ignore second agent definition with same name
			reportError(MASError.AGENT_DUPLICATE_NAME, ctx.ID(), agentName);
		}

		for (final ChannelClauseContext channelClause : ctx.channelClause()) {
			final Map.Entry<String, SubscriptionType> sub = visitChannelClause(channelClause);
			if (sub == null) {
				reportError(MASError.INVALID_CHANNEL, channelClause, "");
			} else {
				agentDf.addSubscription(sub.getKey(), sub.getValue());
			}
		}

		// Add agent definition to MAS program
		getProgram().addAgentDefinition(agentDf);

		return null; // Java says must return something even when Void
	}

	/**
	 * Processes the use clauses in an agent definition section.
	 *
	 * @param agentName The name of the agent that is defined.
	 * @param clauses   List of use clauses in the definition section.
	 */
	public AgentDefinition processAgentDefinition(final String agentName, final List<UseClauseContext> clauses,
			final SourceInfo info) {
		final AgentDefinition agentDf = new AgentDefinition(agentName, this.registry, info);
		for (final UseClauseContext useClause : clauses) {
			final UseClause clause = visitUseClause(useClause);
			// Check if something bad went wrong while parsing
			if (clause == null) {
				continue;
			}

			final List<URI> files = clause.getOrResolveReferences();
			if (files.isEmpty()) {
				reportError(MASError.REFERENCE_COULDNOT_FIND, useClause, clause.getReference());
			}

			// Add use clause to agent definition
			// VALIDATE: Check for duplicate init, event, or main modules
			if (!agentDf.addUseClause(clause)) {
				reportError(MASError.USECASE_DUPLICATE, useClause, clause.getReference());
			}
		}
		return agentDf;
	}

	@Override
	public UseClause visitUseClause(final UseClauseContext ctx) {
		final String typeString = visitUseCase(ctx.useCase());
		final Map.Entry<String, String> ref = visitRef(ctx.ref());

		final UseCase type = UseCase.getUseCase(typeString);
		// Something bad went wrong while parsing
		if (type == null) {
			reportError(MASError.USECASE_INVALID, ctx, typeString, ref.getKey());
			return null;
		} else {
			return new UseClause(ref.getKey(), ref.getValue(), type, getPathRelativeToSourceFile(""),
					getSourceInfo(ctx));
		}
	}

	@Override
	public String visitUseCase(final UseCaseContext ctx) {
		return (ctx == null) ? "" : ctx.getText();
	}

	@Override
	public Map.Entry<String, SubscriptionType> visitChannelClause(final ChannelClauseContext ctx) {
		final String channelCase = (ctx.channelCase() == null) ? null : visitChannelCase(ctx.channelCase());
		final Object constant = (ctx.constant() == null) ? null : visitConstant(ctx.constant());
		if (channelCase != null && constant != null) {
			return new SimpleEntry<>(constant.toString(), SubscriptionType.valueOf(channelCase.toUpperCase()));
		} else {
			return null;
		}
	}

	@Override
	public String visitChannelCase(final ChannelCaseContext ctx) {
		return (ctx == null) ? "" : ctx.getText();
	}

	// -------------------------------------------------------------
	// LAUNCH POLICY section
	// -------------------------------------------------------------

	@Override
	public Object visitPolicy(final PolicyContext ctx) {
		for (final LaunchRuleContext rule : ctx.launchRule()) {
			visitLaunchRule(rule);
		}

		final List<ValidatorWarning> warnings = new ArrayList<>();
		final List<LaunchRule> rules = getProgram().getLaunchRules();
		final SourceInfo info = getSourceInfo(ctx);

		warnings.addAll(checkAgentsUsed(rules, info, getProgram().getAgentNames()));
		if (getProgram().hasEnvironment()) {
			warnings.addAll(checkEnvironmentUsed(rules, info));
		}
		warnings.addAll(checkLaunchRulesReachable(rules, info));
		// Missing agent definitions handled while processing launch
		// instructions

		warnings.stream().forEach(this::reportWarning);
		return null; // Java says must return something even when Void
	}

	@Override
	public Void visitLaunchRule(final LaunchRuleContext ctx) {
		// Process conditional part of launch rule.
		Entity entity = null;
		if (ctx.entity() != null) {
			entity = visitEntity(ctx.entity());
		}

		// Process instructions of launch rule.
		final List<LaunchInstruction> instructions = new ArrayList<>(ctx.instruction().size());
		for (final InstructionContext instruction : ctx.instruction()) {
			instructions.add(visitInstruction(instruction));
		}
		final LaunchRule rule = new LaunchRule(entity, instructions);

		// VALIDATE: A launch rule cannot have an empty list of launch
		// instructions
		if (!instructions.isEmpty()) {
			getProgram().addLaunchRule(rule);
		}

		// VALIDATE: An unconditional launch rule should not use wild cards
		for (final LaunchInstruction launch : rule.getInstructions()) {
			if (!rule.isConditional() && launch.getGivenName(STAR, 0).equals(STAR)) {
				reportError(MASError.LAUNCH_INVALID_WILDCARD, ctx,
						ctx.getText().substring(0, ctx.getText().length() - 1));
			}
		}

		if (rule.isConditional() && !getProgram().hasEnvironment()) {
			reportWarning(MASWarning.LAUNCH_CONDITIONAL_RULE, ctx);
		}

		return null; // Java says must return something even when Void
	}

	@Override
	public Entity visitEntity(final EntityContext ctx) {
		final Entity entity = new Entity();

		if (ctx.entityname() != null) {
			entity.setName(visitEntityname(ctx.entityname()));
		}
		if (ctx.entitytype() != null) {
			entity.setType(visitEntitytype(ctx.entitytype()));
		}

		return entity;
	}

	@Override
	public String visitEntitytype(final EntitytypeContext ctx) {
		return (ctx == null || ctx.ID() == null) ? "" : ctx.ID().getText();
	}

	@Override
	public String visitEntityname(final EntitynameContext ctx) {
		return (ctx == null || ctx.ID() == null) ? "" : ctx.ID().getText();
	}

	@Override
	public LaunchInstruction visitInstruction(final InstructionContext ctx) {
		final String agentName = (ctx.ID() == null) ? "" : ctx.ID().getText();
		final LaunchInstruction instruction = new LaunchInstruction(agentName);

		// Resolve agent reference.
		final AgentSymbol symbol = (AgentSymbol) this.agentNames.resolve(agentName);
		// VALIDATE: Check if definition is available for name
		if (symbol == null) {
			reportError(MASError.LAUNCH_MISSING_AGENTDF, ctx, agentName);
		} else {
			instruction.addAgentDf(symbol.getAgentDf());
		}

		// Process constraints.
		int namec = 0, nrc = 0, maxc = 0;
		for (final ConstraintContext constraintctx : ctx.constraint()) {
			final Map.Entry<String, Object> constraint = visitConstraint(constraintctx);
			instruction.addConstraint(constraint);
			switch (constraint.getKey()) {
			case "name":
				++namec;
				break;
			case "nr":
				++nrc;
				break;
			case "max":
				++maxc;
				break;
			}
		}
		// VALIDATE: Check for duplicate constraints
		if (namec > 1 || nrc > 1 || maxc > 1) {
			reportWarning(MASWarning.CONSTRAINT_DUPLICATE, ctx, ctx.getText().substring(0, ctx.getText().length() - 1));
		}

		// VALIDATE: Check whether given names in launch rules might result in
		// naming conflicts.
		final String name = instruction.getGivenName(null, 0);
		if ((name != null && name != agentName) && (this.agentNames.resolve(name) != null)) {
			// Record potential naming conflict or redundant use of name
			reportError(MASError.AGENT_DUPLICATE_GIVENNAME, ctx.ID(), name);
		}

		return instruction;
	}

	@Override
	public Map.Entry<String, Object> visitConstraint(final ConstraintContext ctx) {
		String key = "";
		Object value = null;

		if (ctx.nameconstraint() != null) {
			key = "name";
			value = visitNameconstraint(ctx.nameconstraint());
		} else if (ctx.nrconstraint() != null) {
			key = "nr";
			value = visitNrconstraint(ctx.nrconstraint());
		} else if (ctx.maxconstraint() != null) {
			key = "max";
			value = visitMaxconstraint(ctx.maxconstraint());
		}

		return new AbstractMap.SimpleEntry<>(key, value);
	}

	@Override
	public String visitNameconstraint(final NameconstraintContext ctx) {
		if (ctx.STAR() != null) {
			return STAR;
		} else if (ctx.ID() != null) {
			return ctx.ID().getText();
		} else {
			return "";
		}
	}

	@Override
	public Integer visitNrconstraint(final NrconstraintContext ctx) {
		return (ctx.INT() == null) ? 0 : Integer.parseInt(ctx.INT().getText().trim());
	}

	@Override
	public Integer visitMaxconstraint(final MaxconstraintContext ctx) {
		return (ctx.INT() == null) ? 0 : Integer.parseInt(ctx.INT().getText().trim());
	}

	@Override
	public String visitString(final StringContext ctx) {
		final StringBuilder str = new StringBuilder();
		if (ctx.StringLiteral() != null) {
			for (final TerminalNode literal : ctx.StringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)\"", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\\"", "\""));
				}
			}
		}
		if (ctx.SingleQuotedStringLiteral() != null) {
			for (final TerminalNode literal : ctx.SingleQuotedStringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)'", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\'", "'"));
				}
			}
		}
		return str.toString();
	}

	/**
	 * Validation of MAS program does not require second pass to resolve any
	 * references.
	 *
	 * TODO: second pass should check for unused declarations.
	 */
	@Override
	protected ValidatorSecondPass createSecondPass() {
		return null;
	}

	public Analysis process() {
		return process(null);
	}

	/**
	 * Call this method after the validator has been run to process the references
	 * in the agent definitions and, recursively, the references to other modules
	 * and KR files.
	 * <p>
	 * This method adds all (syntax) errors and warnings detected while processing
	 * the references to the lists of (syntax) errors and warnings collected while
	 * validating the MAS file. This means, for example, that the
	 * {@link Validator#getErrors()} method will not only return the MAS errors but
	 * also the validation errors reported for references files.
	 * </p>
	 *
	 * @param override {@link Entry} with override settings
	 *
	 * @throws ParserException
	 */
	public Analysis process(final Map.Entry<File, String> override) {
		final MASProgram mas = getProgram();
		final Analysis analysis = new Analysis(this.registry, mas, override);
		analysis.run();
		if (this.registry.hasAnyError()) {
			return analysis;
		}

		// Fully parse all agents that we can find in the MAS,
		// i.e. the modules that are referenced, and all use clauses that
		// are referenced in those modules in turn (if any).
		for (final String agentName : mas.getAgentNames()) {
			final AgentDefinition agentDef = mas.getAgentDefinition(agentName);
			// then parse any potential module use cases with KR parameters
			KRInterface kri = null;
			if (agentDef.getInitUseClause() != null) {
				try {
					processModuleTerms(agentDef.getInitModule(), agentDef.getInitUseClause().getRawParameters(),
							agentDef.getInitUseClause().getSourceInfo());
				} catch (final ParserException e) {
					reportParsingException(e);
				}
				kri = agentDef.getInitModule().getKRInterface();
			}
			if (agentDef.getEventUseClause() != null) {
				try {
					processModuleTerms(agentDef.getEventModule(), agentDef.getEventUseClause().getRawParameters(),
							agentDef.getEventUseClause().getSourceInfo());
				} catch (final ParserException e) {
					reportParsingException(e);
				}
				kri = agentDef.getEventModule().getKRInterface();
			}
			if (agentDef.getMainUseClause() != null) {
				try {
					processModuleTerms(agentDef.getMainModule(), agentDef.getMainUseClause().getRawParameters(),
							agentDef.getMainUseClause().getSourceInfo());
				} catch (final ParserException e) {
					reportParsingException(e);
				}
				kri = agentDef.getMainModule().getKRInterface();
			}
			if (agentDef.getShutdownUseClause() != null) {
				try {
					processModuleTerms(agentDef.getShutdownModule(), agentDef.getShutdownUseClause().getRawParameters(),
							agentDef.getShutdownUseClause().getSourceInfo());
				} catch (final ParserException e) {
					reportParsingException(e);
				}
			}
			if (kri == null) {
				continue;
			}
			try { // check if the channel subscription are valid
				final CognitiveKR cognitiveKR = CognitiveKRFactory.getCognitiveKR(kri);
				for (final String channel : agentDef.getSubscriptions().keySet()) {
					try {
						final List<Term> args = cognitiveKR.visitArguments(channel, agentDef.getSourceInfo());
						if (args.size() != 1 || !cognitiveKR.isSignature(args.get(0))) {
							reportError(MASError.INVALID_CHANNEL, agentDef.getSourceInfo(), channel);
						}
					} catch (final ParserException e) {
						reportParsingException(e);
					}
				}
			} catch (final InstantiationFailedException e) {
				reportParsingException(new ParserException(e.getMessage(), agentDef.getSourceInfo()));
			}
		}

		// Report
		for (final Module module : analysis.getUnusedModuleDefinitions()) {
			reportWarning(MASWarning.MODULE_UNUSED, module.getDefinition(), module.getSignature());
		}
		for (final UserSpecAction action : analysis.getUnusedActionDefinitions()) {
			reportWarning(MASWarning.ACTION_UNUSED, action.getSourceInfo(), action.getSignature());
		}
		for (final DatabaseFormula kr : analysis.getKRUnused()) {
			reportWarning(MASWarning.PREDICATE_UNUSED, kr.getSourceInfo(), kr.getSignature());
		}

		// Final checks
		for (final DatabaseFormula dbf : analysis.getPredicateDefinitions()) {
			try {
				final CognitiveKR ckr = getCognitiveKR();
				final List<Var> vars = ckr.getAllVariables(dbf);
				final Set<Var> unique = new LinkedHashSet<>(vars);
				for (final Var var : unique) {
					final int occurences = Collections.frequency(vars, var);
					if (occurences < 2) {
						reportWarning(MASWarning.VARIABLE_UNUSED, var.getSourceInfo(), var.toString());
					}
				}
			} catch (final ParserException e) {
				reportParsingException(e);
			}
		}
		return analysis;
	}

	private void processModuleTerms(final Module module, final String rawParams, final SourceInfo source)
			throws ParserException {
		try {
			final KRInterface kri = module.getKRInterface();
			final CognitiveKR cognitiveKR = CognitiveKRFactory.getCognitiveKR(kri);
			final List<Term> params = (rawParams == null) ? new ArrayList<>(0)
					: cognitiveKR.visitArguments(rawParams, source);
			final List<Var> vars = module.getParameters();
			final int varCount = vars.size();
			if (varCount == params.size()) {
				final Map<Var, Term> subMap = new LinkedHashMap<>(varCount);
				for (int i = 0; i < varCount; i++) {
					subMap.put(vars.get(i), params.get(i));
				}
				final Substitution sub = kri.getSubstitution(subMap);
				module.setDefaultSubstitution(sub);
			} else {
				reportError(MASError.INVALID_ARGUMENTS, source, (rawParams == null) ? "" : rawParams);
			}
		} catch (final InstantiationFailedException e) {
			throw new ParserException(e.getMessage(), source);
		}
	}

	/**
	 * @param expressions
	 * @return map with defined signatures as key and {@link SourceInfo} as value.
	 * @throws ParserException
	 */
	protected Map<String, SourceInfo> getDefinedSignatures(final Collection<DatabaseFormula> formulas)
			throws ParserException {
		final CognitiveKR ckr = getCognitiveKR();
		final Map<String, SourceInfo> signatures = new LinkedHashMap<>(formulas.size());
		for (final DatabaseFormula formula : formulas) {
			for (final String signature : ckr.getDefinedSignatures(formula)) {
				signatures.put(signature, formula.getSourceInfo());
			}
		}
		return signatures;
	}

	/**
	 * @param formulas
	 * @return map with all explicitly declared signatures as key and all
	 *         {@link SourceInfo} as value.
	 * @throws ParserException
	 */
	protected Map<String, SourceInfo> getDeclaredSignatures(final Collection<DatabaseFormula> formulas)
			throws ParserException {
		final CognitiveKR ckr = getCognitiveKR();
		final Map<String, SourceInfo> signatures = new LinkedHashMap<>(formulas.size());
		for (final DatabaseFormula formula : formulas) {
			for (final String signature : ckr.getDeclaredSignatures(formula)) {
				signatures.put(signature, formula.getSourceInfo());
			}
		}
		return signatures;
	}

	/**
	 * Check that launch rules are not preceded by another launch rule that catches
	 * all entities of the same type. We do not know how entity names and entity
	 * types are related, so we can not check this if a launch rule asks for a
	 * specific name instead of type.
	 *
	 * @param launchRules the launch rules to check
	 * @return list of warning messages.
	 */
	public static List<ValidatorWarning> checkLaunchRulesReachable(final List<LaunchRule> launchRules,
			final SourceInfo info) {
		final List<ValidatorWarning> warnings = new ArrayList<>();
		// a list of type names. If a type is in this list if we encountered a
		// rule that catches all of this type. "*" means all types are caught
		final List<String> finishedTypes = new ArrayList<>();

		for (final LaunchRule rule : launchRules) {
			if (finishedTypes.contains(STAR)) {
				// star-catchall catches everything so nothing can get after
				// it..
				warnings.add(
						new ValidatorWarning(MASWarning.PREV_LAUNCHRULE_CATCHALL_OF_TYPE, info, rule.toString(), STAR));
				continue;
			}
			if (!rule.isConditional()) {
				continue;
			}
			final Entity entity = rule.getEntity();
			if (entity.getName() == null) { // if name=null, this works by type
				String type = entity.getType();
				if (type == null) {
					type = STAR;
				}
				if (finishedTypes.contains(type)) {
					warnings.add(new ValidatorWarning(MASWarning.PREV_LAUNCHRULE_CATCHALL_OF_TYPE, info,
							rule.toString(), type));
				}
				if (rule.isCatchAll()) {
					// then this is the last reachable launch rule.
					finishedTypes.add(type);
				}
			}
		}
		return warnings;
	}

	/**
	 * Check that all agent definitions have been used
	 *
	 * @param launchRules the launch rules to check
	 * @param agentNames  all known agent names
	 * @return list of warning messages.
	 */
	private static List<ValidatorWarning> checkAgentsUsed(final List<LaunchRule> launchRules, final SourceInfo info,
			final Set<String> agentNames) {
		final List<ValidatorWarning> warnings = new ArrayList<>();
		final List<String> agentsUsed = new ArrayList<>();
		for (final LaunchRule rule : launchRules) {
			for (final LaunchInstruction instruction : rule.getInstructions()) {
				agentsUsed.add(instruction.getAgentName());
			}
		}
		if (!agentsUsed.containsAll(agentNames)) {
			warnings.add(new ValidatorWarning(MASWarning.AGENT_UNUSED, info));
		}
		return warnings;
	}

	/**
	 * If environment is specified, launch policy section should have conditional
	 * launch rules to connect agents to it.
	 *
	 * @param launchRules the launch rules to check
	 * @return list of warning messages.
	 */
	private static List<ValidatorWarning> checkEnvironmentUsed(final List<LaunchRule> launchRules,
			final SourceInfo info) {
		final List<ValidatorWarning> warnings = new ArrayList<>();
		boolean crule = false;
		for (final LaunchRule rule : launchRules) {
			if (rule.isConditional()) {
				crule = true;
				break;
			}
		}
		if (!crule) {
			warnings.add(new ValidatorWarning(MASWarning.LAUNCH_NO_CONDITIONAL_RULES, info));
		}
		return warnings;
	}
}
