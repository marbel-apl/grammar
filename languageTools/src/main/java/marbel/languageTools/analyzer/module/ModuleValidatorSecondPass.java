/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package marbel.languageTools.analyzer.module;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import marbel.cognitiveKr.CognitiveKR;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.Expression;
import marbel.krInterface.language.Var;
import marbel.languageTools.analyzer.ValidatorSecondPass;
import marbel.languageTools.errors.module.ModuleWarning;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.languageTools.program.agent.rules.Rule;
import marbel.languageTools.symbolTable.ModuleSymbol;
import marbel.languageTools.symbolTable.Symbol;

/**
 * Implements second pass for validating a module.
 */
public class ModuleValidatorSecondPass extends ValidatorSecondPass {
	/**
	 * Program that is outcome of first pass.
	 */
	private final Module program;

	/**
	 * In the second pass, references in the given agent program are resolved and
	 * related semantic checks are performed.
	 *
	 * <p>
	 * Assumes that the first pass has been performed and the resulting agent
	 * program does not contain any {@code null} references.
	 * </p>
	 * <p>
	 * Any validation errors or warnings are reported.
	 * </p>
	 *
	 * @param firstPass The validator object that executed the first pass.
	 */
	protected ModuleValidatorSecondPass(final ModuleValidator firstPass) {
		super(firstPass);
		this.program = firstPass.getProgram();
	}

	/**
	 * Performs the validation and resolution of references by a walk over the
	 * program structure.
	 */
	@Override
	public void validate() {
		// Process use clause references.
		preProcess();
		preProcessSymbolTables();

		// VALIDATE: Check whether a (single) KR language is used in all
		// referenced files, including module and action specification files.
		// Abort if that is not the case.
		if (!checkKRIuse() || this.program.getRegistry().hasSyntaxError()) { // Abort.
			return;
		}

		// Resolve references (do this after initializing action symbol table).
		resolveReferences(); // FIXME: move into validator itself (first pass)

		// Collect all info needed for validation.
		processInfo();

		// Report variables that should be bound but are not.
		reportUnboundVariables();

		// Report unused items.
		reportUnusedUndefined();
		reportUnusedOrFaultyImports();
		reportUnusedVariables();
	}

	/**
	 * Reports any variables that should have been bound but are not.
	 */
	protected void reportUnboundVariables() {
		// Report variables in rule heads that should have been bound.
		for (final Rule rule : this.program.getRules()) {
			reportUnboundVariables(rule, new LinkedHashSet<>(this.program.getParameters()));
		}
	}

	protected void reportUnusedVariables() {
		for (final Rule rule : this.program.getRules()) {
			reportUnusedVariables(rule, this.program.getParameters());
		}
	}

	private void reportUnusedVariables(final Rule rule, final List<Var> bound) {
		try {
			final CognitiveKR ckr = getFirstPass().getCognitiveKR();
			final List<Var> vars = getAllRuleVariables(rule, ckr);
			final Set<Var> unique = new LinkedHashSet<>(vars);
			unique.removeAll(bound);
			for (final Var var : unique) {
				final int occurences = Collections.frequency(vars, var);
				if (occurences < 2) {
					getFirstPass().reportWarning(ModuleWarning.VARIABLE_UNUSED, var.getSourceInfo(), var.toString());
				}
			}
			for (final Action<?> action : rule.getAction().getActions()) {
				if (action instanceof ModuleCallAction && ((ModuleCallAction) action).getTarget().isAnonymous()) {
					for (final Rule ruleR : ((ModuleCallAction) action).getTarget().getRules()) {
						final List<Var> boundR = new ArrayList<>(bound);
						boundR.addAll(ckr.getAllVariables(rule.getCondition().getFormula()));
						reportUnusedVariables(ruleR, boundR);
					}
				}
			}
		} catch (final ParserException e) {
			getFirstPass().reportParsingException(e);
		}
	}

	private static List<Var> getAllRuleVariables(final Rule rule, final CognitiveKR ckr) throws ParserException {
		final List<Var> vars = new ArrayList<>();
		if (rule.getCondition() != null) {
			vars.addAll(ckr.getAllVariables(rule.getCondition().getFormula()));
		}
		if (rule.getAction() != null) {
			for (final Action<? extends Expression> action : rule.getAction()) {
				if (action instanceof ModuleCallAction) {
					final Module anon = ((ModuleCallAction) action).getTarget();
					if (anon != null && anon.isAnonymous()) {
						for (final Rule subrule : anon.getRules()) {
							vars.addAll(getAllRuleVariables(subrule, ckr));
						}
					}
				}
				for (final Expression param : action.getParameters()) {
					vars.addAll(ckr.getAllVariables(param));
				}
			}
		}
		return vars;
	}

	/**
	 * Processes referenced files in use clauses. Also initializes action symbol
	 * table.
	 * <p>
	 * Parses referenced KR files, and validates referenced module and action
	 * specification files, if these have not been processed yet. We need the
	 * contents of these files to be able to:
	 * <ul>
	 * <li>Resolve user-specified actions (add pre- and post-conditions);</li>
	 * <li>Resolve module calls (add targeted modules);</li>
	 * <li>Verify that predicates used have also been defined.</li>
	 * </ul>
	 * </p>
	 */
	private void preProcessSymbolTables() {
		// Fill action symbol table: add the module itself & referenced modules
		final List<Module> modules = this.program.getUsedModules();
		modules.add(this.program);
		for (final Module module : modules) {
			final String signature = module.getSignature();
			final Symbol symbol = new ModuleSymbol(signature, module, module.getSourceInfo());
			if (!this.knownModules.define(symbol)) {
				reportDuplicateActionLabelDfs(signature, symbol);
			}
		}
	}

	/**
	 * Resolves references in module.
	 */
	private void resolveReferences() {
		final Set<Var> moduleVars = new LinkedHashSet<>(this.program.getParameters());
		for (final Rule rule : this.program.getRules()) {
			resolveReferences(rule, moduleVars);
		}
	}

	/**
	 * Extracts relevant info for validation.
	 */
	private void processInfo() {
		// Extract relevant info from referenced files.
		this.krFormulas.addAll(this.program.getKR());

		// Extract relevant info from rules.
		for (final Rule rule : this.program.getRules()) {
			processInfoRule(rule);
		}
	}
}