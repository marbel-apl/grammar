/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.analyzer.module;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import marbel.cognitiveKr.CognitiveKR;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Term;
import marbel.krInterface.language.Update;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.InputStreamPosition;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.Validator;
import marbel.languageTools.analyzer.ValidatorSecondPass;
import marbel.languageTools.errors.module.ModuleError;
import marbel.languageTools.errors.module.ModuleErrorStrategy;
import marbel.languageTools.errors.module.ModuleWarning;
import marbel.languageTools.parser.MOD2GLexer;
import marbel.languageTools.parser.MOD2GParser;
import marbel.languageTools.parser.MOD2GParser.ActionContext;
import marbel.languageTools.parser.MOD2GParser.ActioncomboContext;
import marbel.languageTools.parser.MOD2GParser.BuiltinContext;
import marbel.languageTools.parser.MOD2GParser.ExitoptionContext;
import marbel.languageTools.parser.MOD2GParser.ModuleContext;
import marbel.languageTools.parser.MOD2GParser.OptionContext;
import marbel.languageTools.parser.MOD2GParser.OrderoptionContext;
import marbel.languageTools.parser.MOD2GParser.RefContext;
import marbel.languageTools.parser.MOD2GParser.RulesContext;
import marbel.languageTools.parser.MOD2GParser.StringContext;
import marbel.languageTools.parser.MOD2GParser.UseclauseContext;
import marbel.languageTools.parser.MOD2GParserVisitor;
import marbel.languageTools.program.Program;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.Module.ExitCondition;
import marbel.languageTools.program.agent.Module.RuleEvaluationOrder;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.agent.actions.CancelTimerAction;
import marbel.languageTools.program.agent.actions.DeleteAction;
import marbel.languageTools.program.agent.actions.ExitModuleAction;
import marbel.languageTools.program.agent.actions.InsertAction;
import marbel.languageTools.program.agent.actions.LogAction;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.languageTools.program.agent.actions.PrintAction;
import marbel.languageTools.program.agent.actions.SendAction;
import marbel.languageTools.program.agent.actions.SleepAction;
import marbel.languageTools.program.agent.actions.StartTimerAction;
import marbel.languageTools.program.agent.actions.SubscribeAction;
import marbel.languageTools.program.agent.actions.UnsubscribeAction;
import marbel.languageTools.program.agent.actions.UserSpecOrModuleCall;
import marbel.languageTools.program.agent.rules.ForallDoRule;
import marbel.languageTools.program.agent.rules.IfThenRule;
import marbel.languageTools.program.agent.rules.Rule;

/**
 * Validates a module file and constructs a module.
 */
public class ModuleValidator extends Validator<MOD2GLexer, MOD2GParser, ModuleErrorStrategy, Module>
		implements MOD2GParserVisitor<Object> {
	private MOD2GParser parser;
	private static ModuleErrorStrategy strategy = null;

	/**
	 * Creates validator for file with file name.
	 *
	 * @param filename Name of a file.
	 */
	public ModuleValidator(final String filename, final FileRegistry registry) {
		super(filename, registry);
	}

	@Override
	protected ParseTree startParser() {
		return this.parser.module();
	}

	@Override
	protected ModuleErrorStrategy getTheErrorStrategy() {
		if (strategy == null) {
			strategy = new ModuleErrorStrategy();
		}
		return strategy;
	}

	/**
	 * Validation of module that resolves references to action, macro, and module
	 * symbols, and checks whether all predicates used have been defined.
	 */
	@Override
	protected ValidatorSecondPass createSecondPass() {
		return new ModuleValidatorSecondPass(this);
	}

	@Override
	protected MOD2GLexer getNewLexer(final CharStream stream) {
		return new MOD2GLexer(stream);
	}

	@Override
	protected MOD2GParser getNewParser(final TokenStream stream) {
		this.parser = new MOD2GParser(stream);
		return this.parser;
	}

	@Override
	protected Module getNewProgram(final File file) throws IOException {
		return new Module(this.registry, new InputStreamPosition(0, 0, 0, 0, file.getCanonicalPath()));
	}

	@Override
	public Module getProgram() {
		return (Module) super.getProgram();
	}

	/**
	 * Calls {@link ParseTree#accept} on the specified tree.
	 */
	@Override
	public Void visit(final ParseTree tree) {
		tree.accept(this);
		return null; // Java says must return something even when Void
	}

	// -------------------------------------------------------------
	// Module
	// -------------------------------------------------------------

	@Override
	public Void visitModule(final ModuleContext ctx) {
		final Program program = getProgram();
		// Process use clauses.
		for (final UseclauseContext useclausectx : ctx.useclause()) {
			visitUseclause(useclausectx);
		}

		// VALIDATE: Check which (single) KR language is used.
		// Based on KR file references only.
		final boolean resolved = program.resolveKRInterface();
		// get list of uri-s from referenced kr stuff
		final List<UseClause> krClauses = program.getKRUseClauses();
		final List<URI> uris = new ArrayList<>();
		for (final UseClause krClause : krClauses) {
			uris.addAll(krClause.getOrResolveReferences());
		}
		if (!resolved) {
			if (uris.isEmpty()) {
				reportError(ModuleError.KR_COULDNOT_RESOLVE_NO_KR_USECLAUSES, ctx);
			} else {
				reportError(ModuleError.KR_COULDNOT_RESOLVE_DIFFERENT_KRS_USED, ctx);
			}
			return null; // makes no sense to go on without a KRI
		}

		// Process options: exit, order.
		for (final OptionContext optionctx : ctx.option()) {
			visitOption(optionctx);
		}

		// Get module name.
		if (ctx.ID() != null) {
			final String name = ctx.ID().getText();
			getProgram().setName(name);
			final SourceInfo definition = getSourceInfo(ctx.ID());
			getProgram().setDefinition(definition);
			if (!FilenameUtils.getBaseName(definition.getSource()).equals(name)) {
				reportWarning(ModuleWarning.MODULE_NAME_MISMATCH, ctx.ID());
			}
		}

		// Get module parameters.
		if (ctx.PARLIST() != null) {
			final List<Var> vars = visitVARLIST(ctx.PARLIST(), ctx);
			getProgram().setParameters(vars);
			// VALIDATE: Check for duplicate parameters.
			final Set<Var> parameterCheck = new HashSet<>(vars.size());
			for (final Var var : vars) {
				if (parameterCheck.contains(var)) {
					reportError(ModuleError.DUPLICATE_PARAMETER, ctx, var.toString());
				} else {
					parameterCheck.add(var);
				}
			}
		}

		// Process rules. Record variables that are used.
		final Set<Var> used = new LinkedHashSet<>();
		for (final RulesContext rulectx : ctx.rules()) {
			final Rule rule = visitRules(rulectx);
			if (rule != null) {
				getProgram().addRule(rule);
				used.addAll(rule.getCondition().getFreeVar());
				for (final Action<?> action : rule.getAction()) {
					used.addAll(action.getFreeVar());
				}
			}
		}

		// VALIDATE: Check for parameters that are never used.
		for (final Var var : getProgram().getParameters()) {
			if (!used.contains(var)) {
				reportWarning(ModuleWarning.PARAMETER_NEVER_USED, ctx, var.toString());
			}
		}

		return null; // Java says must return something even when Void
	}

	@Override
	public String visitRef(final RefContext ctx) {
		if (ctx == null) {
			return "";
		} else if (ctx.string() != null) {
			return visitString(ctx.string());
		} else {
			String path = "";
			for (final String component : ctx.getText().split("\\.")) {
				path = FilenameUtils.concat(path, component);
			}
			return path;
		}
	}

	@Override
	public Void visitUseclause(final UseclauseContext ctx) {
		for (final RefContext ref : ctx.ref()) {
			// Get reference.
			final String reference = visitRef(ref);

			// Create use clause and resolve reference.
			final UseClause useClause = new UseClause(reference, null, null, getPathRelativeToSourceFile(""),
					getSourceInfo(ref));
			final List<URI> files = useClause.getOrResolveReferences();
			if (files.isEmpty()) {
				reportError(ModuleError.REFERENCE_COULDNOT_FIND, ref, reference);
			}

			// Add use clause to module.
			if (!getProgram().addUseClause(useClause)) {
				reportError(ModuleError.REFERENCE_DUPLICATE, ref, reference);
			}
		}

		return null; // Java says must return something even when Void
	}

	@Override
	public Void visitOption(final OptionContext ctx) {
		if (ctx.exitoption() != null) {
			visitExitoption(ctx.exitoption());
		} else if (ctx.orderoption() != null) {
			visitOrderoption(ctx.orderoption());
		}

		return null; // Java says must return something even when Void
	}

	@Override
	public Void visitExitoption(final ExitoptionContext ctx) {
		if (ctx.value != null) {
			try {
				if (!getProgram().setExitCondition(ExitCondition.valueOf(ctx.value.getText().toUpperCase()))) {
					// Report warning if duplicate.
					reportWarning(ModuleWarning.DUPLICATE_OPTION, ctx, "exit");
				}
			} catch (final IllegalArgumentException e) {
				// Simply ignore, parser will report problem.
			}
		}
		return null; // Java says must return something even when Void
	}

	@Override
	public Void visitOrderoption(final OrderoptionContext ctx) {
		final String order = (ctx == null || ctx.value == null) ? "" : ctx.value.getText().toUpperCase();
		try {
			if (getProgram().getRuleEvaluationOrder() == null) {
				getProgram().setRuleEvaluationOrder(RuleEvaluationOrder.valueOf(order));
			} else {
				reportWarning(ModuleWarning.DUPLICATE_OPTION, ctx, "order");
			}
		} catch (final IllegalArgumentException e) {
			// simply ignore, parser will report problem
		}
		return null; // Java says must return something even when Void
	}

	@Override
	public Rule visitRules(final RulesContext ctx) {
		final SourceInfo source = getSourceInfo(ctx);

		// Get action combo.
		ActionCombo combo = null;
		if (ctx.actioncombo() != null) {
			combo = visitActioncombo(ctx.actioncombo());
		}

		// Nested rules: create anonymous module.
		if (ctx.rules() != null && !ctx.rules().isEmpty()) {
			final Module module = new Module(this.registry, source);
			// Inherit KR interface and rule evaluation order.
			module.setKRInterface(getProgram().getKRInterface());
			module.setRuleEvaluationOrder(getProgram().getRuleEvaluationOrder());
			// The module needs a name (for learning)
			module.setName(getProgram().getName() + "_" + module.getSourceInfo().getLineNumber());
			module.setAnonymous();

			for (final RulesContext rulectx : ctx.rules()) {
				module.addRule(visitRules(rulectx));
			}
			getProgram().getMap().merge(module.getMap());

			final ModuleCallAction action = new ModuleCallAction(module, new ArrayList<Term>(0),
					module.getSourceInfo());
			combo = new ActionCombo(action.getSourceInfo());
			combo.addAction(action);
		}

		// Construct rule and add to module.
		if (combo != null) {
			final TerminalNode msc = (ctx.IF() == null) ? ctx.KR_FORALLDO() : ctx.KR_IFTHEN();
			if (msc != null) {
				try {
					final CognitiveKR ckr = getCognitiveKR();
					final String mscText = msc.getText();
					final String toReplace = (ctx.IF() == null) ? "do" : "then";
					final int replacePos = mscText.lastIndexOf(toReplace);
					final String krFragment = mscText.substring(0, replacePos).trim();
					if (!krFragment.isEmpty()) {
						final SourceInfo mscInfo = getSourceInfo(msc);
						final SourceInfo krInfo = (mscInfo == null) ? null
								: new InputStreamPosition(mscInfo.getLineNumber(), mscInfo.getCharacterPosition() + 1,
										mscInfo.getStartIndex() + 1, mscInfo.getStopIndex() - toReplace.length(),
										mscInfo.getSource());
						final Query bel = ckr.visitQuery(krFragment, krInfo);
						reportEmbeddedLanguageErrors(ckr);
						if (bel != null) {
							final MentalLiteral condition = new MentalLiteral(bel, ckr.getUsedSignatures(bel), mscInfo);
							return (ctx.IF() == null) ? new ForallDoRule(condition, combo, source)
									: new IfThenRule(condition, combo, source);
						}
					}
				} catch (final ParserException e) {
					reportParsingException(e);
				}
			}
		}

		return null;
	}

	@Override
	public ActionCombo visitActioncombo(final ActioncomboContext ctx) {
		final ActionCombo combo = new ActionCombo(getSourceInfo(ctx));
		for (final ActionContext actionCtx : ctx.action()) {
			final Action<?> action = visitAction(actionCtx);
			if (action == null) {
				reportError(ModuleError.ACTION_INVALID, ctx, actionCtx.getText());
			} else {
				combo.addAction(action);
				// VALIDATE: Check whether action can be reached.
				if (combo.size() > 1 && combo.getActions().get(combo.size() - 2) instanceof ExitModuleAction) {
					reportWarning(ModuleWarning.EXITMODULE_CANNOT_REACH, actionCtx, action.toString());
				}
			}
		}
		return combo;
	}

	@Override
	public Action<?> visitAction(final ActionContext ctx) {
		if (ctx.ID() != null) {
			// User-defined or module call action.
			final List<Term> parameters = visitPARLIST(ctx.PARLIST(), getSourceInfo(ctx.PARLIST()));
			return new UserSpecOrModuleCall(ctx.ID().getText(), parameters, getSourceInfo(ctx));
		} else if (ctx.builtin() != null) {
			return visitBuiltin(ctx.builtin());
		} else {
			return null;
		}
	}

	@Override
	public Action<?> visitBuiltin(final BuiltinContext ctx) {
		final SourceInfo info = getSourceInfo(ctx);
		if (ctx.op != null && ctx.op.getType() == MOD2GParser.EXITMODULE) {
			return new ExitModuleAction(info);
		}
		// Get KR content.
		final String krFragment = (ctx.PARLIST() == null) ? "" : removeLeadTrailCharacters(ctx.PARLIST().getText());
		if (!krFragment.isEmpty() && ctx.op != null) {
			// Get action operator
			SourceInfo krInfo = getSourceInfo(ctx.PARLIST());
			krInfo = (krInfo == null) ? null
					: new InputStreamPosition(krInfo.getLineNumber(), krInfo.getCharacterPosition() + 1,
							krInfo.getStartIndex() + 1, krInfo.getStopIndex() - 1, krInfo.getSource());
			try {
				Action<?> returned = null;
				final CognitiveKR ckr = getCognitiveKR();
				switch (ctx.op.getType()) {
				case MOD2GParser.INSERT:
					final Update insert = ckr.visitInsert(krFragment, krInfo);
					if (insert != null) {
						returned = new InsertAction(insert, info);
					}
					break;
				case MOD2GParser.DELETE:
					final Update delete = ckr.visitDelete(krFragment, krInfo);
					if (delete != null) {
						returned = new DeleteAction(delete, info);
					}
					break;
				case MOD2GParser.SEND:
					final List<Term> sendParams = ckr.visitArguments(krFragment, krInfo);
					if (sendParams != null) {
						returned = new SendAction(sendParams, info);
					}
					break;
				case MOD2GParser.LOG:
					final List<Term> logParams = ckr.visitArguments(krFragment, krInfo);
					if (logParams != null && !logParams.isEmpty()) {
						returned = new LogAction(logParams, info);
					}
					break;
				case MOD2GParser.PRINT:
					final List<Term> printParams = ckr.visitArguments(krFragment, krInfo);
					if (printParams != null && !printParams.isEmpty()) {
						returned = new PrintAction(printParams, info);
					}
					break;
				case MOD2GParser.SLEEP:
					final List<Term> sleepParams = ckr.visitArguments(krFragment, krInfo);
					if (sleepParams != null && sleepParams.size() == 1) {
						returned = new SleepAction(sleepParams.get(0), info);
					}
					break;
				case MOD2GParser.SUBSCRIBE:
					final List<Term> subscribeParams = ckr.visitArguments(krFragment, krInfo);
					if (subscribeParams != null && !subscribeParams.isEmpty()) {
						returned = new SubscribeAction(subscribeParams, info);
					}
					break;
				case MOD2GParser.UNSUBSCRIBE:
					final List<Term> unsubscribeParams = ckr.visitArguments(krFragment, krInfo);
					if (unsubscribeParams != null && !unsubscribeParams.isEmpty()) {
						returned = new UnsubscribeAction(unsubscribeParams, info);
					}
					break;
				case MOD2GParser.STARTTIMER:
					final List<Term> startTimerParams = ckr.visitArguments(krFragment, krInfo);
					if (startTimerParams != null && startTimerParams.size() == 3) {
						returned = new StartTimerAction(startTimerParams, info);
					}
					break;
				case MOD2GParser.CANCELTIMER:
					final List<Term> cancelTimerParams = ckr.visitArguments(krFragment, krInfo);
					if (cancelTimerParams != null && cancelTimerParams.size() == 1) {
						returned = new CancelTimerAction(cancelTimerParams, info);
					}
					break;
				default:
					break;
				}
				reportEmbeddedLanguageErrors(ckr);
				return returned;
			} catch (final ParserException e) {
				reportParsingException(e);
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public String visitString(final StringContext ctx) {
		final StringBuilder str = new StringBuilder();
		if (ctx.StringLiteral() != null) {
			for (final TerminalNode literal : ctx.StringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)\"", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\\"", "\""));
				}
			}
		}
		if (ctx.SingleQuotedStringLiteral() != null) {
			for (final TerminalNode literal : ctx.SingleQuotedStringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)'", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\'", "'"));
				}
			}
		}
		return str.toString();
	}

	/**
	 * @param token A token index (can be found in GOAL grammar)
	 * @return The name of the token.
	 */
	public static String getTokenName(final int token) {
		return StringUtils.remove(MOD2GParser.VOCABULARY.getDisplayName(token), '\'');
	}
}
