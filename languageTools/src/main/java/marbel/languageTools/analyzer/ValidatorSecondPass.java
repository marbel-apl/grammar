/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.analyzer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKrFactory.CognitiveKRFactory;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.krFactory.KRFactory;
import marbel.krInterface.KRInterface;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Update;
import marbel.krInterface.language.Var;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.Analyzer;
import marbel.languageTools.analyzer.test.TestValidator;
import marbel.languageTools.errors.Message;
import marbel.languageTools.errors.ParserError.SyntaxError;
import marbel.languageTools.errors.module.ModuleError;
import marbel.languageTools.errors.module.ModuleWarning;
import marbel.languageTools.program.Program;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.UseClause.UseCase;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.agent.actions.DeleteAction;
import marbel.languageTools.program.agent.actions.InsertAction;
import marbel.languageTools.program.agent.actions.ModuleCallAction;
import marbel.languageTools.program.agent.actions.UserSpecAction;
import marbel.languageTools.program.agent.actions.UserSpecOrModuleCall;
import marbel.languageTools.program.agent.rules.Rule;
import marbel.languageTools.program.kr.KRProgram;
import marbel.languageTools.symbolTable.ModuleSymbol;
import marbel.languageTools.symbolTable.Symbol;
import marbel.languageTools.symbolTable.SymbolTable;

public abstract class ValidatorSecondPass {
	/**
	 * First pass over parse tree.
	 */
	private final Validator<?, ?, ?, ?> firstPass;
	/**
	 * The program constructed during the first pass.
	 */
	private final Program program;
	/**
	 * All known module signatures
	 */
	protected final SymbolTable knownModules = new SymbolTable();
	/**
	 * Action and macro labels used.
	 */
	protected final Set<String> actionsDefined = new LinkedHashSet<>();
	protected final Set<String> actionLabelsUsed = new LinkedHashSet<>();
	/**
	 * All belief updates used.
	 */
	protected final List<Update> krUpdates = new ArrayList<>();
	/**
	 * The beliefs specified in the module.
	 */
	protected final List<DatabaseFormula> krFormulas = new ArrayList<>();
	/**
	 * The queries on the belief base that occur in the module.
	 */
	protected final List<Query> krQueries = new ArrayList<>();
	// the updates from insert actions
	private final List<Update> insertUpdates = new ArrayList<>();
	// the updates from delete actions
	private final List<Update> deleteUpdates = new ArrayList<>();

	/**
	 * In the second pass, references in the given program are resolved and semantic
	 * checks are performed.
	 *
	 * <p>
	 * Assumes that the first pass has been performed.
	 * </p>
	 * <p>
	 * Reports validation errors and warnings.
	 * </p>
	 *
	 * @param firstPass The validator object that executed the first pass.
	 */
	protected ValidatorSecondPass(final Validator<?, ?, ?, ?> firstPass) {
		this.firstPass = firstPass;
		this.program = firstPass.getProgram();
	}

	public Validator<?, ?, ?, ?> getFirstPass() {
		return this.firstPass;
	}

	/**
	 * Performs the validation and resolution of references by a walk over the
	 * program structure.
	 */
	public abstract void validate();

	/**
	 * Processes referenced files in use clauses.
	 * <p>
	 * Parses referenced KR files, and validates referenced module and action
	 * specification files, if these have not been processed yet. We need the
	 * contents of these files to be able to:
	 * <ul>
	 * <li>Resolve user-specified actions (add pre- and post-conditions);</li>
	 * <li>Resolve module calls (add targeted modules);</li>
	 * <li>Verify that predicates used have also been defined.</li>
	 * </ul>
	 * </p>
	 */
	public void preProcess() {
		for (final UseClause useClause : this.firstPass.getProgram().getUseClauses()) {
			for (final File source : useClause.getReferencedFiles()) {
				if (this.firstPass.getRegistry().needsProcessing(source)) {
					if (useClause.getUseCase() == UseCase.KR_FILE) {
						parseKRfile(source, useClause.getSourceInfo());
					} else {
						try {
							Analyzer.processFile(source, this.firstPass.getRegistry());
						} catch (final IOException e) {
							this.firstPass.reportParsingException(new ParserException(e.getMessage(), source));
						}
					}
				}
			}
		}
	}

	/**
	 * Check whether a (single) KR language is used in all referenced files,
	 * including module and action specification files. If not, report first
	 * mismatch and abort. Also abort if KR interface could not be resolved for this
	 * module.
	 *
	 * @return {@code true} if one and the same KR language is used.
	 */
	public boolean checkKRIuse() {
		final KRInterface kri = this.program.getKRInterface();
		if (kri == null) {
			return false;
		} else {
			for (final UseClause useClause : this.program.getUseClauses()) {
				if (useClause.getUseCase() != UseCase.KR_FILE) {
					for (final File file : useClause.getReferencedFiles()) {
						final Program program = this.firstPass.getRegistry().getProgram(file);
						if (program != null && program.getKRInterface() != kri) {
							this.firstPass.reportError(ModuleError.KR_USE_OF_DIFFERENT_KRIS, useClause.getSourceInfo(),
									KRFactory.getName(kri), useClause.getReferencedFiles().toString(),
									KRFactory.getName(program.getKRInterface()));
							return false;
						}
					}
				}
			}
			return true;
		}
	}

	/**
	 * Report unused KR expressions.
	 */
	public void validateKR() {
		final List<Query> undefinedQueries = this.program.getKRInterface().getUndefined(getKRFormulas(),
				getKRQueries());
		for (final Query query : undefinedQueries) {
			this.firstPass.reportError(ModuleError.KR_QUERIED_NEVER_DEFINED, query.getSourceInfo(),
					query.toString());
		}
	}

	/**
	 * Parses a KR file.
	 *
	 * @param source  A KR file.
	 * @param useCase Use case for file.
	 * @param info    Source info.
	 */
	private void parseKRfile(final File source, final SourceInfo info) {
		final KRInterface kri = this.program.getKRInterface();
		if (kri != null) {
			try {
				final CognitiveKR ckr = CognitiveKRFactory.getCognitiveKR(kri);
				final List<DatabaseFormula> formulas = ckr.visitFile(source);

				final KRProgram program = new KRProgram(this.firstPass.getRegistry(), formulas, info);
				this.firstPass.getRegistry().register(source, program);
				for (final SourceInfo error : ckr.getErrors()) {
					this.firstPass.reportError(SyntaxError.EMBEDDED_LANGUAGE_ERROR, error,
							KRFactory.getName(this.program.getKRInterface()), error.getMessage());
				}
				if (formulas.isEmpty() && !program.getRegistry().hasSyntaxError()) {
					this.firstPass.reportWarning(ModuleWarning.EMPTY_FILE, info, source.toString());
				}
			} catch (final ParserException e) {
				this.firstPass.reportParsingException(e);
			} catch (final InstantiationFailedException e) {
				this.firstPass
						.reportParsingException(new ParserException("failed to parse '" + source + "'.", info, e));
			}
		}
	}

	/**
	 * Reports error if a variable in head of rule that should be bound is not.
	 */
	protected void reportUnboundVariables(final Rule rule, final Set<Var> bound) {
		final Set<Var> boundedVars = new LinkedHashSet<>(bound);
		boundedVars.addAll(rule.getCondition().getFreeVar());
		// Check each action in the rule
		for (final Action<?> action : rule.getAction().getActions()) {
			if (action == null) {
			} else if (action instanceof ModuleCallAction && ((ModuleCallAction) action).getTarget().isAnonymous()) {
				// Only check nested rules, do not recursively enter other
				// modules!
				for (final Rule ruleR : ((ModuleCallAction) action).getTarget().getRules()) {
					reportUnboundVariables(ruleR, boundedVars);
				}
			} else {
				final Set<Var> newScope = new LinkedHashSet<>(boundedVars);
				final Set<Var> freeActionParVars = new LinkedHashSet<>(action.getFreeVar());
				freeActionParVars.removeAll(newScope);
				if (!freeActionParVars.isEmpty()) {
					getFirstPass().reportError(ModuleError.ACTION_CALL_UNBOUND_VARIABLE, action.getSourceInfo(),
							getFirstPass().prettyPrintSet(freeActionParVars), action.getName());
				}
			}
		}
	}

	protected void reportUnusedUndefined() {
		// Report undefined KR expressions.
		validateKR();
	}

	/**
	 * Reports any import ('use ...') that is not defining predicates used in the
	 * module or that contains errors.
	 */
	protected void reportUnusedOrFaultyImports() {
		final FileRegistry registry = getFirstPass().getRegistry();
		final Map<String, String> allErrors = new HashMap<>();
		for (final Message error : registry.getAllErrors()) {
			final String source = error.getSource().getSource();
			String existing = allErrors.get(source);
			if (existing == null) {
				existing = "";
			} else {
				existing += ", ";
			}
			allErrors.put(source, existing + error.toShortString());
		}

		for (final UseClause useClause : getFirstPass().getProgram().getUseClauses()) {
			for (final File file : useClause.getReferencedFiles()) {
				final Program program = registry.getProgram(file);
				if (useClause.getUseCase() == UseCase.KR_FILE) {
					reportIfUnusedDatabase(useClause, ((KRProgram) program).getDBFormulas());
				}
				try {
					final String errors = allErrors.get(file.getCanonicalPath());
					if (errors != null) {
						getFirstPass().reportError(ModuleError.REFERENCE_HAS_ERRORS, program.getSourceInfo(),
								useClause.getReference(), errors);
					}
				} catch (final IOException ignore) {
				}
			}
		}
	}

	/**
	 * Check if the given database formulas from the useClause contains at least one
	 * useful definition
	 *
	 * @param useClause  the reference to the imported file.
	 * @param dbFormulas the {@link DatabaseFormula}s in the file.
	 */
	private void reportIfUnusedDatabase(final UseClause useClause, final List<DatabaseFormula> dbFormulas) {
		// TODO
	}

	/**
	 * Checks whether action label is defined in different references (source files)
	 * and reports error if that is the case.
	 *
	 * @param signature Signature of the action or module.
	 * @param symbol    Symbol that already was present in action symbol table.
	 */
	protected void reportDuplicateActionLabelDfs(final String signature, final Symbol symbol) {
		// Check if duplicate in different source files.
		final Symbol duplicate = this.knownModules.resolve(signature);
		final String symbolSource = symbol.getSourceInfo().getSource();
		final String duplicateSource = duplicate.getSourceInfo().getSource();
		if (!symbolSource.equals(duplicateSource)) {
			// TODO: should point to references in program file instead of
			// actual file names.
			getFirstPass().reportError(ModuleError.ACTION_LABEL_DEFINED_BY_MULTIPLE_REFERENCES,
					this.program.getSourceInfo(), signature, symbolSource, duplicateSource);
		}
	}

	/**
	 * Resolves references to macros in a rule condition and references to action
	 * specifications and/or modules in the head of a rule.
	 *
	 * @param rule     A decision rule.
	 * @param usedVars the vars that are already in use (parameters of the module).
	 *                 These are needed to rename vars inside macro calls.
	 */
	protected void resolveReferences(final Rule rule, final Set<Var> usedVars) {
		final Set<Var> contextVars = new LinkedHashSet<>(usedVars);
		contextVars.addAll(rule.getCondition().getFreeVar());
		for (final Action<?> action : rule.getAction()) {
			// include variables in potential sub-rules here too, otherwise
			// those might
			// still conflict with a substitution coming from a macro
			if (action instanceof ModuleCallAction && ((ModuleCallAction) action).getTarget().isAnonymous()) {
				for (final Rule sub : ((ModuleCallAction) action).getTarget().getRules()) {
					contextVars.addAll(sub.getCondition().getFreeVar());
				}
			}
		}
		resolveActionReferences(rule.getAction(), contextVars);
	}

	/**
	 * Resolves references to action specifications and/or modules.
	 *
	 * @param usedActions An action combo.
	 * @param usedVars    All variables used in the context of the call. needed to
	 *                    rename macro calls.
	 */
	protected void resolveActionReferences(final ActionCombo usedActions, final Set<Var> usedVars) {
		final List<Action<?>> resolvedActions = new ArrayList<>();
		for (final Action<?> action : usedActions.getActions()) {
			// Resolve reference to user specified action or module call.
			if (action instanceof UserSpecOrModuleCall) {
				final Action<?> resolved = resolveActionReferences((UserSpecOrModuleCall) action);
				if (resolved != null) {
					resolvedActions.add(resolved);
				}
			} else {
				if (action instanceof ModuleCallAction) {
					// Process nested rules.
					final Module module = ((ModuleCallAction) action).getTarget();
					if (module.isAnonymous()) {
						for (final Rule rule : module.getRules()) {
							resolveReferences(rule, usedVars);
						}
					}
				}
				resolvedActions.add(action);
			}
		}

		// Substitute resolved references.
		usedActions.setActions(resolvedActions);
	}

	/**
	 * Resolve references in user-specified or module call action.
	 *
	 * @param usedAction A user-specified or module call action.
	 * @return Resolved action.
	 */
	private Action<?> resolveActionReferences(final UserSpecOrModuleCall usedAction) {
		final Symbol symbol = this.knownModules.resolve(usedAction.getSignature());
		if (symbol instanceof ModuleSymbol) {
			final Module target = ((ModuleSymbol) symbol).getModule();
			final SourceInfo call = (getFirstPass() instanceof TestValidator) ? target.getDefinition()
					: target.getSourceInfo();
			return new ModuleCallAction(target, usedAction.getParameters(), call);
		} else { // always assume external action for now
			return new UserSpecAction(usedAction.getName(), usedAction.getParameters(), usedAction.getSourceInfo());
		}
		// getFirstPass().reportError(ModuleError.ACTION_USED_NEVER_DEFINED,
		// usedAction.getSourceInfo(), signature);
	}

	protected void processInfoRule(final Rule rule) {
		processInfoMsc(rule.getCondition());
		processInfoCombo(rule.getAction());
	}

	/**
	 * Extracts queries on the belief base (including belief, a-goal, and goal-a
	 * literals).
	 *
	 * @param msc A mental state condition.
	 */
	protected void processInfoMsc(final MentalLiteral msc) {
		this.krQueries.add(msc.getFormula());
	}

	/**
	 * Extracts dynamically inserted beliefs and goals. Also records which action
	 * labels have been used.
	 *
	 * @param combo An action combo.
	 */
	protected void processInfoCombo(final ActionCombo combo) {
		for (final Action<?> action : combo) {
			if (action == null) {
				continue;
			}
			// Keep track of action labels that have been used.
			this.actionLabelsUsed.add(action.getSignature());
			// Get items inserted by insert action.
			if (action instanceof InsertAction) {
				final Update update = ((InsertAction) action).getUpdate();
				this.krUpdates.add(update);
				this.krQueries.add(update.toQuery());
				this.insertUpdates.add(update);
			}
			// Get items inserted by delete action.
			if (action instanceof DeleteAction) {
				final Update update = ((DeleteAction) action).getUpdate();
				this.krUpdates.add(update);
				this.krQueries.add(update.toQuery());
				this.deleteUpdates.add(update);
			}
			// Process nested rules.
			if (action instanceof ModuleCallAction) {
				final Module module = ((ModuleCallAction) action).getTarget();
				if (module.isAnonymous()) {
					for (final Rule ruleR : module.getRules()) {
						processInfoRule(ruleR);
					}
				}
			}
		}
	}

	public List<DatabaseFormula> getKRFormulas() {
		return Collections.unmodifiableList(this.krFormulas);
	}

	public List<Query> getKRQueries() {
		return Collections.unmodifiableList(this.krQueries);
	}

	public Set<String> getActionsUsed() {
		return this.actionLabelsUsed;
	}

	/**
	 *
	 * @return all updates done in insert actions
	 */
	public List<Update> getInsertUpdates() {
		return Collections.unmodifiableList(this.insertUpdates);
	}

	/**
	 *
	 * @return all updates done in delete actions
	 */
	public List<Update> getDeleteUpdates() {
		return Collections.unmodifiableList(this.deleteUpdates);
	}

	/**
	 *
	 * @return all belief update actions found in the program. Both coming from
	 *         explicit module actions and from action specs.
	 */
	public List<Update> getKRUpdates() {
		return Collections.unmodifiableList(this.krUpdates);
	}
}
