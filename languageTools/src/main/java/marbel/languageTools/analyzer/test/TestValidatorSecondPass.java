/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.analyzer.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import marbel.cognitiveKr.CognitiveKR;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.Var;
import marbel.languageTools.analyzer.ValidatorSecondPass;
import marbel.languageTools.errors.test.TestWarning;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.UseClause.UseCase;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.agent.actions.UserSpecOrModuleCall;
import marbel.languageTools.program.agent.rules.Rule;
import marbel.languageTools.program.kr.KRProgram;
import marbel.languageTools.program.test.AgentTest;
import marbel.languageTools.program.test.ModuleTest;
import marbel.languageTools.program.test.TestAction;
import marbel.languageTools.program.test.TestProgram;
import marbel.languageTools.program.test.testcondition.TestCondition;
import marbel.languageTools.symbolTable.ModuleSymbol;
import marbel.languageTools.symbolTable.Symbol;

public class TestValidatorSecondPass extends ValidatorSecondPass {
	/**
	 * Program that is outcome of first pass.
	 */
	private final TestProgram program;

	/**
	 * In the second pass, references in the given agent program are resolved and
	 * related semantic checks are performed.
	 *
	 * <p>
	 * Assumes that the first pass has been performed and the resulting agent
	 * program does not contain any {@code null} references.
	 * </p>
	 * <p>
	 * Any validation errors or warnings are reported.
	 * </p>
	 *
	 * @param firstPass The validator object that executed the first pass.
	 */
	public TestValidatorSecondPass(final TestValidator firstPass) {
		super(firstPass);
		this.program = firstPass.getProgram();
	}

	/**
	 * Performs the validation and resolution of references by a walk over the
	 * program structure.
	 */
	@Override
	public void validate() {
		// Process use clause references.
		preProcess();
		preProcessSymbolTables();

		// VALIDATE: Check whether a (single) KR language is used in all
		// referenced files, including module and action specification files.
		// Abort if that is not the case.
		if (!checkKRIuse() || this.program.getRegistry().hasSyntaxError()) { // Abort.
			return;
		}

		// Resolve references (do this after initializing action symbol table).
		resolveReferences(); // FIXME: move into validator itself (first pass)

		// Collect all info needed for validation.
		processInfo();

		// Report unused items.
		reportUnusedUndefined();
		reportUnusedVariables();
	}

	/**
	 * Processes referenced files in use clauses. Also initializes action symbol
	 * table.
	 * <p>
	 * Parses referenced KR files, and validates referenced module and action
	 * specification files, if these have not been processed yet. We need the
	 * contents of these files to be able to:
	 * <ul>
	 * <li>Resolve user-specified actions (add pre- and post-conditions);</li>
	 * <li>Resolve module calls (add targeted modules);</li>
	 * <li>Verify that predicates used have also been defined.</li>
	 * </ul>
	 * </p>
	 */
	private void preProcessSymbolTables() {
		// Fill action symbol table: Add action specifications. Check for
		// duplicates.
		for (final UseClause clause : this.program.getUseClauses()) {
			for (final File file : clause.getReferencedFiles()) {
				switch (clause.getUseCase()) {
				case MODULE:
					// Fill action symbol table: Add referenced modules.
					final Module module = (Module) this.program.getRegistry().getProgram(file);
					final String signature = module.getSignature();
					final Symbol symbol = new ModuleSymbol(signature, module, module.getSourceInfo());
					if (!this.knownModules.define(symbol)) {
						reportDuplicateActionLabelDfs(signature, symbol);
					}
					break;
				default:
					break;
				}
			}
		}
	}

	/**
	 * Resolves references in module.
	 */
	private void resolveReferences() {
		final List<ActionCombo> actions = new ArrayList<>();
		final List<MentalLiteral> conditions = new ArrayList<>();
		for (final AgentTest agent : this.program.getAgentTests()) {
			for (final TestAction action : agent.getActions()) {
				actions.add(action.getAction());
				if (action.getCondition() != null && action.getCondition().getQuery() != null) {
					conditions.add(action.getCondition().getQuery());
				}
			}
		}
		for (final ModuleTest module : this.program.getModuleTests()) {
			final ActionCombo stubCombo = new ActionCombo(null);
			final Action<?> stub = new UserSpecOrModuleCall(module.getModuleName(), module.getModuleArguments(),
					module.getSourceInfo());
			stubCombo.addAction(stub);
			actions.add(stubCombo);
			for (final TestCondition maintest : module.getTestConditions()) {
				TestCondition test = maintest;
				while (test != null) {
					final MentalLiteral condition = (test == null) ? null : test.getQuery();
					if (condition != null) {
						conditions.add(condition);
					}
					test = test.getNestedCondition();
				}
			}
		}

		final Set<Var> contextVars = new LinkedHashSet<>(0);
		for (final ActionCombo actioncombo : actions) {
			resolveActionReferences(actioncombo, contextVars);
			processInfoCombo(actioncombo);
		}
		for (final MentalLiteral condition : conditions) {
			processInfoMsc(condition);
		}
	}

	protected void reportUnusedVariables() {
		for (final ModuleTest test : this.program.getModuleTests()) {
			try {
				final CognitiveKR ckr = getFirstPass().getCognitiveKR();
				for (final TestCondition condition : test.getTestConditions()) {
					final List<Var> vars = getVariablesInTestCondition(condition, ckr);
					final Set<Var> unique = new LinkedHashSet<>(vars);
					unique.removeAll(test.getModuleArguments());
					for (final Var var : unique) {
						final int occurences = Collections.frequency(vars, var);
						if (occurences < 2) {
							getFirstPass().reportWarning(TestWarning.VARIABLE_UNUSED, var.getSourceInfo(),
									var.toString());
						}
					}
				}
			} catch (final ParserException e) {
				getFirstPass().reportParsingException(e);
			}
		}
	}

	private static List<Var> getVariablesInTestCondition(final TestCondition condition, final CognitiveKR ckr)
			throws ParserException {
		final List<Var> vars = ckr.getAllVariables(condition.getQuery().getFormula());
		if (condition.hasNestedCondition()) {
			vars.addAll(getVariablesInTestCondition(condition.getNestedCondition(), ckr));
		}
		return vars;
	}

	/**
	 * Extracts relevant info for validation.
	 */
	private void processInfo() {
		for (final UseClause clause : this.program.getUseClauses()) {
			for (final File file : clause.getReferencedFiles()) {
				if (clause.getUseCase() == UseCase.KR_FILE) {
					final KRProgram krProgram = (KRProgram) this.program.getRegistry().getProgram(file);
					this.krFormulas.addAll(krProgram.getDBFormulas());
				}
			}
		}
	}

	@Override
	protected void processInfoRule(final Rule rule) {
		processInfoCombo(rule.getAction());
	}
}
