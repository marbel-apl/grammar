/**

 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.analyzer.test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.io.FilenameUtils;

import marbel.cognitiveKr.CognitiveKR;
import marbel.krInterface.exceptions.ParserException;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Term;
import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.InputStreamPosition;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.Validator;
import marbel.languageTools.analyzer.ValidatorSecondPass;
import marbel.languageTools.analyzer.mas.MASValidator;
import marbel.languageTools.errors.ParserError.SyntaxError;
import marbel.languageTools.errors.module.ModuleError;
import marbel.languageTools.errors.test.TestError;
import marbel.languageTools.errors.test.TestErrorStrategy;
import marbel.languageTools.errors.test.TestWarning;
import marbel.languageTools.parser.TEST2GLexer;
import marbel.languageTools.parser.TEST2GParser;
import marbel.languageTools.parser.TEST2GParser.ActionContext;
import marbel.languageTools.parser.TEST2GParser.AgenttestContext;
import marbel.languageTools.parser.TEST2GParser.ModulerefContext;
import marbel.languageTools.parser.TEST2GParser.ModuletestContext;
import marbel.languageTools.parser.TEST2GParser.ReacttestContext;
import marbel.languageTools.parser.TEST2GParser.RefContext;
import marbel.languageTools.parser.TEST2GParser.RunconditionContext;
import marbel.languageTools.parser.TEST2GParser.StringContext;
import marbel.languageTools.parser.TEST2GParser.TemporaltestContext;
import marbel.languageTools.parser.TEST2GParser.TestContext;
import marbel.languageTools.parser.TEST2GParser.TestactionContext;
import marbel.languageTools.parser.TEST2GParser.TestconditionContext;
import marbel.languageTools.parser.TEST2GParser.TimeoutoptionContext;
import marbel.languageTools.parser.TEST2GParser.UseclauseContext;
import marbel.languageTools.parser.TEST2GParserVisitor;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.UseClause.UseCase;
import marbel.languageTools.program.agent.MentalLiteral;
import marbel.languageTools.program.agent.actions.Action;
import marbel.languageTools.program.agent.actions.ActionCombo;
import marbel.languageTools.program.agent.actions.UserSpecOrModuleCall;
import marbel.languageTools.program.mas.MASProgram;
import marbel.languageTools.program.test.AgentTest;
import marbel.languageTools.program.test.ModuleTest;
import marbel.languageTools.program.test.TestAction;
import marbel.languageTools.program.test.TestProgram;
import marbel.languageTools.program.test.testcondition.Always;
import marbel.languageTools.program.test.testcondition.Eventually;
import marbel.languageTools.program.test.testcondition.Never;
import marbel.languageTools.program.test.testcondition.TestCondition;
import marbel.languageTools.program.test.testcondition.Until;

/**
 * Validates a test file and constructs a test program.
 */
public class TestValidator extends Validator<TEST2GLexer, TEST2GParser, TestErrorStrategy, TestProgram>
		implements TEST2GParserVisitor<Object> {
	private TEST2GParser parser;
	private MASProgram override;
	private static TestErrorStrategy strategy = null;

	/**
	 * Creates the test validator.
	 *
	 * @param filename Name of file which contains test.
	 */
	public TestValidator(final String filename, final FileRegistry registry) {
		super(filename, registry);
	}

	@Override
	protected TEST2GLexer getNewLexer(final CharStream stream) {
		return new TEST2GLexer(stream);
	}

	@Override
	protected TEST2GParser getNewParser(final TokenStream stream) {
		this.parser = new TEST2GParser(stream);
		return this.parser;
	}

	@Override
	protected ParseTree startParser() {
		return this.parser.test();
	}

	@Override
	protected TestErrorStrategy getTheErrorStrategy() {
		if (strategy == null) {
			strategy = new TestErrorStrategy();
		}
		return strategy;
	}

	@Override
	protected TestProgram getNewProgram(final File file) throws IOException {
		return new TestProgram(this.registry, new InputStreamPosition(0, 0, 0, 0, file.getCanonicalPath()));
	}

	@Override
	public TestProgram getProgram() {
		return (TestProgram) super.getProgram();
	}

	@Override
	protected ValidatorSecondPass createSecondPass() {
		return new TestValidatorSecondPass(this);
	}

	public void overrideMAS(final MASProgram mas) {
		this.override = mas;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation calls {@link ParseTree#accept} on the specified
	 * tree.
	 * </p>
	 */
	@Override
	public Void visit(final ParseTree tree) {
		tree.accept(this);
		return null; // Java says must return something even when Void
	}

	@Override
	public Void visitTest(final TestContext ctx) {
		if (ctx == null) {
			return null;
		}
		try {
			// Process use clauses (mas2g).
			for (final UseclauseContext useclausectx : ctx.useclause()) {
				visitUseclause(useclausectx);
			}
			File masPath = null;
			for (final UseClause clause : getProgram().getUseClauses()) {
				final List<File> resolved = clause.getReferencedFiles();
				if (clause.getUseCase() == UseCase.MAS && resolved.size() == 1) {
					masPath = resolved.get(0);
					break;
				}
			}
			if (masPath == null) {
				reportError(TestError.MAS_MISSING, ctx);
				return null;
			}
			// Process option (timeout).
			if (ctx.timeoutoption() != null) {
				visitTimeoutoption(ctx.timeoutoption());
			}

			if (this.override == null) {
				final MASValidator mas2g = new MASValidator(masPath.getCanonicalPath(), this.registry);
				mas2g.validate();
				mas2g.process();
				if (mas2g.getProgram() == null || mas2g.getProgram().getKRInterface() == null) {
					return null; // best effort...
				}
				getProgram().setMAS(mas2g.getProgram());
			} else {
				getProgram().setMAS(this.override);
				for (final File source : this.override.getRegistry().getSourceFiles()) {
					this.registry.register(source, this.override.getRegistry().getProgram(source));
				}
			}

			for (final ModuletestContext testCtx : ctx.moduletest()) {
				final List<ModuleTest> moduleTests = visitModuletest(testCtx);
				for (final ModuleTest moduleTest : moduleTests) {
					if (!getProgram().addModuleTest(moduleTest)) {
						reportWarning(TestWarning.DUPLICATE_MODULE_TEST, testCtx, moduleTest.getModuleSignature());
					}
				}
			}

			for (final AgenttestContext testCtx : ctx.agenttest()) {
				final List<AgentTest> agentTests = visitAgenttest(testCtx);
				for (final AgentTest agentTest : agentTests) {
					if (!getProgram().getMAS().getAgentNames().contains(agentTest.getAgentName())) {
						reportError(TestError.TEST_INVALID_AGENT, testCtx, agentTest.getAgentName());
					} else if (!getProgram().addAgentTest(agentTest)) {
						reportWarning(TestWarning.DUPLICATE_AGENT_TEST, testCtx, agentTest.getAgentName());
					}
				}
			}

			return null;
		} catch (final Exception any) {
			// Convert stack trace to string
			final StringWriter sw = new StringWriter();
			any.printStackTrace(new PrintWriter(sw));
			reportError(SyntaxError.FATAL, null, any.getMessage() + "\n" + sw.toString());
			return null;
		}
	}

	@Override
	public String visitRef(final RefContext ctx) {
		if (ctx == null) {
			return "";
		} else if (ctx.string() != null) {
			return visitString(ctx.string());
		} else {
			String path = "";
			for (final String component : ctx.getText().split("\\.")) {
				path = FilenameUtils.concat(path, component);
			}
			return path;
		}
	}

	@Override
	public Void visitUseclause(final UseclauseContext ctx) {
		for (final RefContext ref : ctx.ref()) {
			// Get reference.
			final String reference = visitRef(ref);

			// Create use clause and resolve reference.
			final UseClause useClause = new UseClause(reference, null, null, getPathRelativeToSourceFile(""),
					getSourceInfo(ref));
			final List<URI> files = useClause.getOrResolveReferences();
			if (files.isEmpty()) {
				reportError(ModuleError.REFERENCE_COULDNOT_FIND, ref, reference);
			}

			// Add use clause to module.
			if (!getProgram().addUseClause(useClause)) {
				reportError(ModuleError.REFERENCE_DUPLICATE, ref, reference);
			}
		}

		return null; // Java says must return something even when Void
	}

	@Override
	public Void visitTimeoutoption(final TimeoutoptionContext ctx) {
		final String number = (ctx.value == null) ? "" : ctx.value.getText();
		try {
			getProgram().setTimeout(Long.parseLong(number));
		} catch (final NumberFormatException e) {
			reportError(TestError.TIMEOUT_INVALID, ctx, number);
		}
		return null;
	}

	@Override
	public List<ModuleTest> visitModuletest(final ModuletestContext ctx) {
		final List<ModuleTest> tests = new ArrayList<>();
		if (ctx.moduleref() == null || ctx.moduleref().isEmpty()) {
			reportError(TestError.TEST_MISSING_MODULE, ctx);
			return tests;
		}

		for (final ModulerefContext module : ctx.moduleref()) {
			final ModuleTest test = visitModuleref(module);
			final List<TestCondition> conditions = new ArrayList<>(ctx.testcondition().size());
			for (final TestconditionContext conditionCtx : ctx.testcondition()) {
				final TestCondition condition = visitTestcondition(conditionCtx);
				if (condition != null) {
					conditions.add(condition);
				}
			}
			test.setTestConditions(conditions);
			tests.add(test);
		}

		return tests;
	}

	@Override
	public ModuleTest visitModuleref(final ModulerefContext ctx) {
		List<Term> params = new ArrayList<>(0);
		if (ctx.PARLIST() != null) {
			params = visitPARLIST(ctx.PARLIST(), getSourceInfo(ctx.PARLIST()));
		}
		return new ModuleTest(ctx.ID().getText(), params, getSourceInfo(ctx.ID()));
	}

	@Override
	public List<AgentTest> visitAgenttest(final AgenttestContext ctx) {
		final List<AgentTest> tests = new ArrayList<>();
		if (ctx.ID() == null || ctx.ID().isEmpty()) {
			reportError(TestError.TEST_MISSING_AGENT, ctx);
			return tests;
		}

		for (final TerminalNode ID : ctx.ID()) {
			final String agentName = ID.getText();
			if (ctx.testaction() == null || ctx.testaction().isEmpty()) {
				tests.add(new AgentTest(agentName, getSourceInfo(ctx)));
			} else {
				final List<TestAction> actions = new ArrayList<>(ctx.testaction().size());
				for (final TestactionContext actionCtx : ctx.testaction()) {
					final TestAction action = visitTestaction(actionCtx);
					if (action != null) {
						actions.add(action);
					}
				}
				tests.add(new AgentTest(agentName, actions, getSourceInfo(ctx)));
			}
		}

		return tests;
	}

	@Override
	public TestAction visitTestaction(final TestactionContext ctx) {
		if (ctx.action() == null) {
			reportError(TestError.TEST_MISSING_ACTION, ctx);
			return null;
		} else {
			final Action<?> action = visitAction(ctx.action());
			if (action == null) {
				return null;
			}
			final ActionCombo combo = new ActionCombo(getSourceInfo(ctx));
			combo.addAction(action);
			if (ctx.runcondition() == null) {
				return new TestAction(combo);
			} else {
				return new TestAction(combo, visitRuncondition(ctx.runcondition()));
			}
		}
	}

	@Override
	public Action<?> visitAction(final ActionContext ctx) {
		if (ctx.ID() != null) {
			// User-defined or module call action.
			final List<Term> parameters = visitPARLIST(ctx.PARLIST(), getSourceInfo(ctx.PARLIST()));
			return new UserSpecOrModuleCall(ctx.ID().getText(), parameters, getSourceInfo(ctx));
		} else {
			return null;
		}
	}

	@Override
	public TestCondition visitTestcondition(final TestconditionContext ctx) {
		if (ctx.temporaltest() != null) {
			final TestCondition query = visitTemporaltest(ctx.temporaltest());
			if (query != null) {
				return query;
			}
		} else if (ctx.reacttest() != null) {
			final TestCondition query = visitReacttest(ctx.reacttest());
			if (query != null) {
				return query;
			}
		}

		return null;
	}

	@Override
	public TestCondition visitReacttest(final ReacttestContext ctx) {
		if (ctx.KR_IF() != null && ctx.KR_THEN() != null) {
			final MentalLiteral first = fromKRIF(ctx.KR_IF());
			final MentalLiteral second = fromKRTHEN(ctx.KR_THEN());
			if (first == null || second == null) {
				return null;
			} else {
				final TestCondition returned = new Always(first, getText(ctx));
				final TestCondition nested = new Eventually(second, getText(ctx));
				returned.setNestedCondition(nested);
				return returned;
			}
		} else {
			reportError(TestError.TEST_MISSING_QUERY, ctx);
			return null;
		}
	}

	@Override
	public TestCondition visitTemporaltest(final TemporaltestContext ctx) {
		final MentalLiteral msc = (ctx.KR_THEN() == null) ? null : fromKRTHEN(ctx.KR_THEN());
		if (msc != null) {
			if (ctx.ALWAYS() != null) {
				return new Always(msc, getText(ctx));
			} else if (ctx.NEVER() != null) {
				return new Never(msc, getText(ctx));
			} else if (ctx.EVENTUALLY() != null) {
				return new Eventually(msc, getText(ctx));
			} else {
				reportError(TestError.TEST_INVALID_OPERATOR, ctx);
				return null;
			}
		} else {
			reportError(TestError.TEST_MISSING_QUERY, ctx);
			return null;
		}
	}

	@Override
	public Until visitRuncondition(final RunconditionContext ctx) {
		final MentalLiteral msc = (ctx.KR_THEN() == null) ? null : fromKRTHEN(ctx.KR_THEN());
		if (msc != null) {
			return new Until(msc, getText(ctx));
		} else {
			reportError(TestError.TEST_MISSING_QUERY, ctx);
			return null;
		}
	}

	@Override
	public String visitString(final StringContext ctx) {
		final StringBuilder str = new StringBuilder();
		if (ctx.StringLiteral() != null) {
			for (final TerminalNode literal : ctx.StringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)\"", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\\"", "\""));
				}
			}
		}
		if (ctx.SingleQuotedStringLiteral() != null) {
			for (final TerminalNode literal : ctx.SingleQuotedStringLiteral()) {
				final String[] parts = literal.getText().split("(?<!\\\\)'", 0);
				if (parts.length > 1) {
					str.append(parts[1].replace("\\'", "'"));
				}
			}
		}
		return str.toString();
	}

	private String getText(final ParserRuleContext ctx) {
		final int start = ctx.start.getStartIndex();
		final int stop = (ctx.stop == null) ? start : ctx.stop.getStopIndex();
		return ctx.getStart().getInputStream().getText(new Interval(start, stop));
	}

	private MentalLiteral fromKRTHEN(final TerminalNode krCtx) {
		MentalLiteral msc = null;
		if (krCtx != null) {
			try {
				final CognitiveKR ckr = getCognitiveKR();
				final String mscText = krCtx.getText();
				final int replacePos = mscText.lastIndexOf(".");
				final String krFragment = mscText.substring(0, replacePos).trim();
				if (!krFragment.isEmpty()) {
					final SourceInfo mscInfo = getSourceInfo(krCtx);
					final SourceInfo krInfo = (mscInfo == null) ? null
							: new InputStreamPosition(mscInfo.getLineNumber(), mscInfo.getCharacterPosition() + 1,
									mscInfo.getStartIndex() + 1, mscInfo.getStopIndex() - 1, mscInfo.getSource());
					final Query bel = ckr.visitQuery(krFragment, krInfo);
					reportEmbeddedLanguageErrors(ckr);
					if (bel != null) {
						msc = new MentalLiteral(bel, ckr.getUsedSignatures(bel), mscInfo);
					}
				}
			} catch (final ParserException e) {
				reportParsingException(e);
			}
		}
		return msc;
	}

	private MentalLiteral fromKRIF(final TerminalNode krCtx) {
		MentalLiteral msc = null;
		if (krCtx != null) {
			try {
				final CognitiveKR ckr = getCognitiveKR();
				final String mscText = krCtx.getText();
				final int replacePos = mscText.lastIndexOf("if");
				final String krFragment = mscText.substring(0, replacePos).trim();
				if (!krFragment.isEmpty()) {
					final SourceInfo mscInfo = getSourceInfo(krCtx);
					final SourceInfo krInfo = (mscInfo == null) ? null
							: new InputStreamPosition(mscInfo.getLineNumber(), mscInfo.getCharacterPosition() + 1,
									mscInfo.getStartIndex() + 1, mscInfo.getStopIndex() - 1, mscInfo.getSource());
					final Query bel = ckr.visitQuery(krFragment, krInfo);
					reportEmbeddedLanguageErrors(ckr);
					if (bel != null) {
						msc = new MentalLiteral(bel, ckr.getUsedSignatures(bel), mscInfo);
					}
				}
			} catch (final ParserException e) {
				reportParsingException(e);
			}
		}
		return msc;
	}
}
