package marbel.languageTools.codeanalysis;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import marbel.cognitiveKr.CognitiveKR;
import marbel.cognitiveKrFactory.CognitiveKRFactory;
import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.krInterface.language.DatabaseFormula;
import marbel.krInterface.language.Query;
import marbel.krInterface.language.Update;
import marbel.languageTools.analyzer.mas.Analysis;
import marbel.languageTools.program.UseClause;
import marbel.languageTools.program.agent.Module;
import marbel.languageTools.program.agent.actions.UserSpecAction;

/**
 * A formatter that turns an {@link Analysis} into an ontology-style text
 * describing the MAS.
 *
 */
public class MasOntology {
	private static final String BR = "\n";
	private final Analysis analysis;
	private final CognitiveKR cognitivekr;

	public MasOntology(final Analysis analysis) throws InstantiationFailedException {
		this.analysis = analysis;
		this.cognitivekr = CognitiveKRFactory.getCognitiveKR(analysis.getProgram().getKRInterface());
	}

	/**
	 *
	 * @param file the file to get short path of
	 * @return short path with only parent directory and the file's filename.
	 */
	private String shortPath(final File file) {
		if (file == null) {
			return "";
		} else {
			final File parent = file.getParentFile();
			return (parent == null) ? file.getName() : (parent.getName() + File.separator + file.getName());
		}
	}

	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder("Analysis of mas: ")
				.append(shortPath(this.analysis.getProgram().getSourceFile())).append(BR);
		str.append("environment: ").append(shortPath(this.analysis.getProgram().getEnvironmentfile())).append(BR);
		str.append("agents: ").append(this.analysis.getProgram().getAgentNames()).append(BR);

		final Set<String> modNames = new TreeSet<>();
		for (final Module module : this.analysis.getModuleDefinitions()) {
			modNames.add("" + module.getName());
		}
		str.append("modules: ").append(modNames).append(BR);

		str.append("KR files:").append(BR);

		for (final UseClause use : this.analysis.getKRFiles()) {
			for (final File f : use.getReferencedFiles()) {
				str.append("  ").append(f.getName()).append(BR);
			}
		}

		str.append("Predicate info:").append(BR);
		// Unused predicates
		final Set<String> unused = new LinkedHashSet<>();
		for (final DatabaseFormula formula : this.analysis.getKRUnused()) {
			// CHECK should we include only the top level formula?
			unused.addAll(this.cognitivekr.getUsedSignatures(formula));
		}
		str.append("  Unused predicates:").append(unused).append(BR);

		// belief queries.
		final Set<String> krPreds = new TreeSet<>();
		for (final Query query : this.analysis.getPredicateQueries()) {
			krPreds.addAll(this.cognitivekr.getUsedSignatures(query));
		}
		str.append("  All KR predicates:").append(setToString(krPreds, 4)).append(BR);

		// insert updates
		final Set<String> insertPreds = new TreeSet<>();
		for (final Update update : this.analysis.getInsertUpdates()) {
			insertPreds.addAll(this.cognitivekr.getUsedSignatures(update));
		}
		str.append("  All insert predicates: ").append(setToString(insertPreds, 4)).append(BR);

		// delete updates
		final Set<String> deletePreds = new TreeSet<>();
		for (final Update update : this.analysis.getDeleteUpdates()) {
			deletePreds.addAll(this.cognitivekr.getUsedSignatures(update));
		}
		str.append("  All delete predicates: ").append(setToString(deletePreds, 4)).append(BR);

		str.append("Action info:").append(BR);
		// notice, an action can be both used and unused. Unfortunately we can't
		// find the used anymore.
		final Set<String> usedActions = new TreeSet<>();
		for (final UserSpecAction action : this.analysis.getActionDefinitions()) {
			usedActions.add(action.getSignature());
		}
		str.append("  Used actions:").append(setToString(usedActions, 4)).append(BR);

		final Set<String> unusedActions = new TreeSet<>();
		for (final UserSpecAction action : this.analysis.getUnusedActionDefinitions()) {
			unusedActions.add(action.getSignature());
		}
		str.append("  Unused actions:").append(setToString(unusedActions, 4)).append(BR);

		str.append("Rule counts").append(BR);
		str.append("  event modules: ").append(ruleCount(this.analysis.getEventModules())).append(BR);
		str.append("  init modules: ").append(ruleCount(this.analysis.getInitModules())).append(BR);

		final List<Module> remaining = new ArrayList<>(this.analysis.getModuleDefinitions());
		remaining.removeAll(this.analysis.getEventModules());
		remaining.removeAll(this.analysis.getInitModules());
		str.append("  other modules:").append(BR);
		for (final Module module : remaining) {
			str.append("    ").append(module.getSignature()).append(":").append(module.getRules().size()).append(BR);
		}

		return str.toString();
	}

	/**
	 * @param modules the modules to count
	 * @return total #rules in the set of modules.
	 */
	private int ruleCount(final List<Module> modules) {
		int ruleCount = 0;
		for (final Module mod : modules) {
			ruleCount += mod.getRules().size();
		}
		return ruleCount;
	}

	/**
	 * @param list        elements to put in the string
	 * @param indentation the indentation for each element
	 * @return string with each element of the list on a new line, prepended with
	 *         #indentation whitespaces
	 */
	private String setToString(final Set<String> list, final int indentation) {
		String ind$ = "\n", res = "";

		for (int n = 0; n < indentation; n++) {
			ind$ += " ";
		}

		for (final String element : list) {
			res += ind$ + element;
		}
		return res;
	}
}
