package marbel.languageTools.codeanalysis;

import java.io.File;
import java.io.IOException;

import marbel.cognitiveKrFactory.InstantiationFailedException;
import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.mas.Analysis;
import marbel.languageTools.analyzer.mas.MASValidator;
import marbel.languageTools.program.mas.MASProgram;

public class MasAnalysis {
	private final Analysis analysis;

	/**
	 * @param program the {@link MASProgram} to be analyzed
	 * @throws InstantiationFailedException if the provided MAS program is not valid
	 */
	public MasAnalysis(final File masFile) throws InstantiationFailedException {
		final FileRegistry registry = new FileRegistry();
		MASValidator mas2g;
		try {
			mas2g = new MASValidator(masFile.getCanonicalPath(), registry);
		} catch (final IOException e) {
			throw new InstantiationFailedException("cannot find MAS file", e);
		}

		mas2g.validate();
		this.analysis = mas2g.process();
		if (registry.hasAnyError()) {
			throw new InstantiationFailedException(masFile + " is not valid: " + registry.getAllErrors());
		}
		if (registry.hasWarning()) {
			System.out.println(registry.getWarnings());
		}
	}

	@Override
	public String toString() {
		try {
			return (this.analysis == null) ? "no analysis available" : new MasOntology(this.analysis).toString();
		} catch (final InstantiationFailedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
