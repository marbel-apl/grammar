/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package marbel.languageTools.symbolTable;

import java.io.File;

import marbel.krInterface.parser.SourceInfo;
import marbel.languageTools.program.mas.AgentDefinition;

/**
 * An agent symbol associates an agent definition with the agent name in an
 * agent definition section in a MAS file.
 */
public class AgentSymbol extends Symbol {

	private final AgentDefinition agentDf;

	public AgentSymbol(final String name, final AgentDefinition agentDf, final SourceInfo info) {
		super(name, info);
		this.agentDf = agentDf;
	}

	/**
	 * @return The agent definition associated with this agent symbol.
	 */
	public AgentDefinition getAgentDf() {
		return this.agentDf;
	}

	/**
	 * @return String representation of this {@link #AgentSymbol(String, File)}.
	 */
	@Override
	public String toString() {
		return "<AgentSymbol: " + getName() + ">";
	}

}
