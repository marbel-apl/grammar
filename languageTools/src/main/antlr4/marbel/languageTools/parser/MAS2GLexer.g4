/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The grammar for MAS files for the GOAL agent programming language.
 * 
 * Operators and punctuation symbols are directly referenced in the grammar to enhance readability.
 * References to <i>tokens</i> thus are implicit in the grammar. The token labels can be found at the
 * end of the grammar.
 */
lexer grammar MAS2GLexer;

tokens{ HIDDEN }

INIT 			: 'init';
UPDATES 		: 'updates';
DECISIONS		: 'decisions';
SHUTDOWN        : 'shutdown';
USE				: 'use';
WITH			: 'with';
DEFINE			: 'define';
FOR				: 'for';
AS				: 'as';
WHEN			: 'when';
LAUNCHPOLICY	: 'launchpolicy';
LAUNCH			: 'launch';
TYPE			: 'type';
NAME			: 'name';
NUMBER			: 'number';
MAX				: 'max';
ADD				: 'add';
DELETE			: 'delete';
REPLACE			: 'replace';
UPDATE			: 'update';

EQUALS	: '=';
MINUS	: '-';
PLUS	: '+';
DOT		: '.';
COMMA	: ',';
LBR		: '(';
RBR		: ')';
CLBR	: '{';
CRBR	: '}';
SLBR	: '[';
SRBR	: ']';

ID
	: (LETTER | SCORE) (LETTER | DIGIT | SLASH | SCORE)*
	;

fragment LETTER: [\p{Alpha}\p{General_Category=Other_Letter}];
fragment SCORE: '_';
fragment SLASH: '/';
fragment DIGIT: [\p{Digit}];

STAR : '*';

FLOAT
	: ('+' | '-')? (DIGIT+ '.' DIGIT+)
	| ('.' DIGIT+)
	;
	
INT
	: ('+' | '-')? DIGIT+
	;
	
// Parameter list of KR terms (anything between brackets)
PARLIST
  :  '(' (  ~('(' | ')') | PARLIST )* ')'
  ;

// White space and comments.
LINE_COMMENT
	: '%' ~[\r\n]* -> channel(HIDDEN)
	;

BLOCK_COMMENT
	: '/*' .*? '*/' -> channel(HIDDEN)
	;

WS
	: WHITESPACECHAR+ -> skip
	;

fragment WHITESPACECHAR: [\p{White_Space}];

fragment EscapedQuote: '\\"';
StringLiteral
	: '"' (EscapedQuote | ~[\r\n"])* '"'
	;

fragment EscapedSingleQuote: '\\\'';
SingleQuotedStringLiteral
	: '\'' (EscapedSingleQuote | ~[\r\n'])* '\''
	;