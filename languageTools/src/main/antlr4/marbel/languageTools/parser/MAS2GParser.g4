/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The grammar for MAS files for the GOAL agent programming language.
 * 
 * Operators and punctuation symbols are directly referenced in the grammar to enhance readability.
 * References to <i>tokens</i> thus are implicit in the grammar. The token labels can be found at the
 * end of the grammar.
 */
parser grammar MAS2GParser;

options{ tokenVocab=MAS2GLexer; }

mas
	: environment? agent* policy EOF
	;

// Environment section

environment
	: USE ref (WITH initKeyValue (',' initKeyValue)*)? '.'
	;
	
ref
	: string | ID PARLIST? | (ID ('.' ID)* '.' ID PARLIST?)
	;

initKeyValue
	: ID '=' initExpr
	;

initExpr
	: (constant | list)
	;

constant
	: ID 
	| INT
	| FLOAT
	| string
	;

list
	: '[' initExpr (',' initExpr)* ']'
	;

// Agent section

agent
	: DEFINE ID '{' (useClause|channelClause)* '}'
	;
 
useClause
	: USE ref FOR useCase '.'
	;

useCase
	: (INIT | UPDATES | DECISIONS | SHUTDOWN)
	;
	
channelClause
	:  channelCase constant '.'
	;
	
channelCase
	: (ADD | DELETE | REPLACE | UPDATE)
	;

// Launch Policy section

policy
	: LAUNCHPOLICY '{' launchRule* '}'
	;

launchRule
	: (WHEN entity)? LAUNCH instruction (',' instruction)* '.'
	;

entity
	: STAR | entitytype | entityname
	;
	
entitytype
	: TYPE '=' ID
	;
	
entityname
	: NAME '=' ID
	;

instruction
	: ID (WITH constraint (',' constraint)* )?
	;
	
constraint
	: nameconstraint | nrconstraint | maxconstraint
	;
	
nameconstraint
	: NAME '=' ID | NAME '=' STAR
	;
	
nrconstraint
	: NUMBER '=' INT
	;
	
maxconstraint
	: MAX '=' INT
	;

string
	: (StringLiteral ('+' StringLiteral)*)
	| (SingleQuotedStringLiteral ('+' SingleQuotedStringLiteral)*)
	;
