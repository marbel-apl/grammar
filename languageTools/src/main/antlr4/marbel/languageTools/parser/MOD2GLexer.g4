/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The grammar for module files for the GOAL agent programming language.
 * 
 * Operators and punctuation symbols are directly referenced in the grammar to enhance readability.
 * References to <i>tokens</i> thus are implicit in the grammar. The token labels can be found at the
 * end of the grammar.
 */
lexer grammar MOD2GLexer;

tokens{ HIDDEN }
	
// Modules
USE				: 'use';
EXITMODULE		: 'exit-module'; // built-in action but up here because of next token
EXIT			: 'exit';
ALWAYS			: 'always';
NEVER			: 'never';
NOACTION		: 'noaction';
MODULE  		: 'module';

// Options
ORDER			: 'order';
LINEARALL		: 'linearall';
LINEARALLRANDOM	: 'linearallrandom';
LINEAR			: 'linear';
LINEARRANDOM	: 'linearrandom';
RANDOMALL		: 'randomall';
RANDOM			: 'random';

// Rule tokens
IF				: 'if' WS 			-> pushMode(IFTHEN);
FORALL			: 'forall' WS		-> pushMode(FORALLDO);

// Built-in actions
INSERT			: 'insert';
DELETE			: 'delete';
LOG				: 'log';
PRINT			: 'print';
SEND			: 'send';
SLEEP			: 'sleep';
UNSUBSCRIBE		: 'unsubscribe';
SUBSCRIBE		: 'subscribe';
STARTTIMER		: 'starttimer';
CANCELTIMER		: 'canceltimer';

fragment EscapedQuote: '\\"';
StringLiteral
	: '"' (EscapedQuote | ~[\r\n"])* '"'
	;

fragment EscapedSingleQuote: '\\\'';
SingleQuotedStringLiteral
	: '\'' (EscapedSingleQuote | ~[\r\n'])* '\''
	;

//  Plus, equals, and punctuation tokens.
PLUS	: '+';
EQUALS	: '=';
DOT		: '.';
COMMA	: ',';
LBR		: '(';
RBR		: ')';
CLBR	: '{';
CRBR	: '}';

// GOAL identifiers
ID
	: (CHAR | SCORE) (CHAR | DIGIT | SCORE)*
	;

fragment CHAR	: [\p{Alpha}\p{General_Category=Other_Letter}];
fragment SCORE	: '_';
fragment DIGIT	: [\p{Digit}];
	
// Parameter list of KR terms (anything between brackets)
PARLIST
  :  '(' (  ~('(' | ')') | PARLIST )* ')'
  ;

// Comments
LINE_COMMENT	: '%' ~[\r\n]*		-> channel(HIDDEN);
BLOCK_COMMENT	: '/*' .*? '*/'		-> channel(HIDDEN);
// White space
WS				: WHITESPACECHAR+	-> channel(HIDDEN);
fragment WHITESPACECHAR: [\p{White_Space}];

// Knowledge representation code
mode IFTHEN;
KR_IFTHEN	: .*? WS 'then' WS 		-> popMode;

mode FORALLDO;
KR_FORALLDO	: .*? WS 'do' WS		-> popMode;