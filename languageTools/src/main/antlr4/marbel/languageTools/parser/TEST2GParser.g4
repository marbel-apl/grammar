/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * The Test parser grammar describes the grammar rules for the tests for GOAL agent programs.
 */
parser grammar TEST2GParser;

options{ tokenVocab=TEST2GLexer; }

@members {
	// This variable decides when to enter KR parsing and when not.
	// if set to TRUE, it tells parser to NOT enter KR parsing.
	boolean stayInDefault = false;
}

useclause
	: USE ref (',' ref)* '.'
	;
	
ref
	: string | (ID ('.' ID)*)
	;
	
timeoutoption
	: TIMEOUT '=' (value = INT) '.'
	;

test
	: useclause* timeoutoption? moduletest* agenttest+ EOF
	;

// ID is the name of a module here
moduletest
	: TEST moduleref (',' moduleref)* '{' testcondition* '}'
	;

moduleref
	: ID PARLIST?
	;
	
testcondition
	: (temporaltest | reacttest)
	;
temporaltest
	: (ALWAYS | NEVER | EVENTUALLY) KR_THEN
	;
reacttest
	: IF KR_IF KR_THEN
	;

// ID is the name of an agent here
agenttest
	: ID (',' ID)* '{' testaction* '}'
	;

testaction
	: DO action (runcondition | '.')
	;
runcondition
    : UNTIL KR_THEN
	;
	
action 
	: ID PARLIST?
	;
	
string
	: (StringLiteral ('+' StringLiteral)*)
	| (SingleQuotedStringLiteral ('+' SingleQuotedStringLiteral)*)
	;
