/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * The Test parser grammar describes the grammar rules for the tests for GOAL agent programs.
 */
lexer grammar TEST2GLexer;

tokens{ HIDDEN }

@members {
	// This variable decides when to enter KR parsing and when not.
	// if set to TRUE, it tells parser to NOT enter KR parsing.
	boolean stayInDefault = false;
}
	
// Use clauses
USE				: 'use';

// Test tokens
TIMEOUT			: 'timeout';
TEST			: 'test';
DONE			: 'done'			{ stayInDefault = true; };
ALWAYS			: 'always' WS		-> pushMode(THEN_CONDITION);
NEVER			: 'never' WS		-> pushMode(THEN_CONDITION);
EVENTUALLY		: 'eventually' WS	-> pushMode(THEN_CONDITION);
IF				: 'if' WS			-> pushMode(IF_CONDITION);
DO				: 'do';
UNTIL		    : WS 'until' WS		-> pushMode(THEN_CONDITION);

fragment EscapedQuote: '\\"';
StringLiteral
	: '"' (EscapedQuote | ~[\r\n"])* '"'
	;

fragment EscapedSingleQuote: '\\\'';
SingleQuotedStringLiteral
	: '\'' (EscapedSingleQuote | ~[\r\n'])* '\''
	;

//  Plus, equals, and punctuation tokens.
PLUS	: '+';
EQUALS	: '=';
DOT		: '.';
COMMA	: ',';
LBR		: '('		 { stayInDefault = false; };
RBR		: ')';
CLBR	: '{';
CRBR	: '}';

// GOAL identifiers

ID
	: (CHAR | SCORE) (CHAR | DIGIT | SCORE)*
	;

fragment CHAR	: [\p{Alpha}\p{General_Category=Other_Letter}];
fragment SCORE	: '_';
fragment DIGIT	: [\p{Digit}];
	
INT
	: DIGIT+
	;
	
// Parameter list of KR terms (anything between brackets)
PARLIST
  :  { !stayInDefault }? '(' (  ~('(' | ')') | PARLIST )* ')'
  ;

// Comments
LINE_COMMENT	: '%' ~[\r\n]*		-> channel(HIDDEN);
BLOCK_COMMENT	: '/*' .*? '*/'		-> channel(HIDDEN);
// White space
WS				: WHITESPACECHAR+	-> channel(HIDDEN);
fragment WHITESPACECHAR: [\p{White_Space}];

// Knowledge representation code
mode THEN_CONDITION;
KR_THEN	: .*? '.' WS 				-> popMode;

mode IF_CONDITION;
KR_IF	: .*? WS 'then' WS			-> popMode, pushMode(THEN_CONDITION);