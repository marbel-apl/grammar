/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * The grammar for module files for the GOAL agent programming language.
 * 
 * Operators and punctuation symbols are directly referenced in the grammar to enhance readability.
 * References to <i>tokens</i> thus are implicit in the grammar. The token labels can be found at the
 * end of the grammar.
 */
parser grammar MOD2GParser;

options{ tokenVocab=MOD2GLexer; }

module
	: (useclause)* (option)* MODULE ID PARLIST? '{' rules* '}' EOF
	;

useclause
	: USE ref (',' ref)* '.'
	;
	
ref
	: string | (ID ('.' ID)*)
	;

option
	: exitoption | orderoption
	;

exitoption
	: EXIT '=' (value = ALWAYS | value = NEVER | value = NOACTION) '.'
	;

orderoption
	: ORDER '='
	  (value = LINEAR | value = LINEARALL | value = LINEARRANDOM | value = LINEARALLRANDOM | value = RANDOM | value = RANDOMALL) '.'
	;
	
rules 
	: IF KR_IFTHEN ( actioncombo '.' | '{' rules* '}' )
	| FORALL KR_FORALLDO ( actioncombo '.' | '{' rules* '}' )
	;
	
actioncombo
	: action ('+' action)*
	;
	
action 
	: ID PARLIST? | builtin
	;
	
builtin
	: op = INSERT PARLIST 
	| op = DELETE PARLIST
	| op = SEND PARLIST 
	| op = LOG PARLIST
	| op = PRINT PARLIST
	| op = SLEEP PARLIST
	| op = UNSUBSCRIBE PARLIST
	| op = SUBSCRIBE PARLIST
	| op = STARTTIMER PARLIST
	| op = CANCELTIMER PARLIST
	| op = EXITMODULE
	;
	
string
	: (StringLiteral ('+' StringLiteral)*)
	| (SingleQuotedStringLiteral ('+' SingleQuotedStringLiteral)*)
	;
