package marbel.languageTools.analyzer.mas;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import marbel.languageTools.analyzer.mas.MASValidator;
import marbel.languageTools.errors.ValidatorWarning;
import marbel.languageTools.program.mas.Entity;
import marbel.languageTools.program.mas.LaunchRule;

/**
 * Test if rules are reachable (part of MasValidator)
 *
 */
public class MasValidatorToolsTest {
	private final String TYPE1 = "type1"; // arbitrary type
	private final String TYPE2 = "type2"; // arbitrary type

	private final LaunchRule simpleRule1 = mock(LaunchRule.class);
	private final LaunchRule simpleRule2 = mock(LaunchRule.class);

	// conditional rule, but type=null (just triggers on a entity name)
	private final LaunchRule conditionalRule1 = mock(LaunchRule.class);
	private final Entity entity = mock(Entity.class);

	private final LaunchRule catchSomeType1 = mock(LaunchRule.class);

	private final LaunchRule catchAllRule1 = mock(LaunchRule.class);
	private final Entity entity1 = mock(Entity.class);

	private final LaunchRule catchAllRule2 = mock(LaunchRule.class);
	private final Entity entity2 = mock(Entity.class);

	private final LaunchRule catchAllStarRule = mock(LaunchRule.class);
	private final Entity entityStar = mock(Entity.class);

	@Before
	public void before() {

		when(this.catchSomeType1.isConditional()).thenReturn(true);
		when(this.catchSomeType1.getEntity()).thenReturn(this.entity1);
		when(this.conditionalRule1.isCatchAll()).thenReturn(false);

		when(this.conditionalRule1.isConditional()).thenReturn(true);
		when(this.conditionalRule1.getEntity()).thenReturn(this.entity);
		when(this.conditionalRule1.isCatchAll()).thenReturn(false);

		when(this.catchAllRule1.isConditional()).thenReturn(true);
		when(this.catchAllRule1.getEntity()).thenReturn(this.entity1);
		when(this.catchAllRule1.isCatchAll()).thenReturn(true);
		when(this.entity1.getType()).thenReturn(this.TYPE1);

		when(this.catchAllRule2.isConditional()).thenReturn(true);
		when(this.catchAllRule2.getEntity()).thenReturn(this.entity2);
		when(this.catchAllRule2.isCatchAll()).thenReturn(true);
		when(this.entity2.getType()).thenReturn(this.TYPE2);

		when(this.catchAllStarRule.isConditional()).thenReturn(true);
		when(this.catchAllStarRule.getEntity()).thenReturn(this.entityStar);
		when(this.catchAllStarRule.isCatchAll()).thenReturn(true);
		when(this.entityStar.getType()).thenReturn(null); // means ALL types.

	}

	@Test
	public void testNoRulesReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(0);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertTrue(warnings.isEmpty());
	}

	@Test
	public void testSimpleRulesReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(2);
		launchRules.add(this.simpleRule1);
		launchRules.add(this.simpleRule2);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertTrue(warnings.isEmpty());
	}

	@Test
	public void testOneConditionalRuleReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(1);
		launchRules.add(this.conditionalRule1);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertTrue(warnings.isEmpty());
	}

	@Test
	public void testOneCatchAllReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(1);
		launchRules.add(this.catchAllRule1);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertTrue(warnings.isEmpty());
	}

	/**
	 * 2 catch-all rules catching the same type
	 */
	@Test
	public void testTwoCatchAllsReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(2);
		launchRules.add(this.catchAllRule1);
		launchRules.add(this.catchAllRule1);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertFalse(warnings.isEmpty());
		assertTrue(warnings.get(0).toString().contains("unreachable because a previous rule catches all entities"));
	}

	/**
	 * 2 catch-all rules but catching differnet types
	 */
	@Test
	public void testTwoCatchAllsDifferentReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(2);
		launchRules.add(this.catchAllRule1);
		launchRules.add(this.catchAllRule2);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertTrue(warnings.isEmpty());
	}

	/**
	 * 2 catch-all rules. rule 1 and 3 catch same type
	 */
	@Test
	public void testThreeCatchAllsReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(3);
		launchRules.add(this.catchAllRule1);
		launchRules.add(this.catchAllRule2);
		launchRules.add(this.catchAllRule1);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertFalse(warnings.isEmpty());
		assertTrue(warnings.get(0).toString().contains("unreachable because a previous rule catches all entities"));
	}

	/**
	 * catch-all rule with * type, followed by a normal rule (which can not be
	 * reached as * catches everything)
	 */
	@Test
	public void testCatchAllAndNormalReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(2);
		launchRules.add(this.catchAllStarRule);
		launchRules.add(this.simpleRule1);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertFalse(warnings.isEmpty());
		assertTrue(warnings.get(0).toString().contains("unreachable because a previous rule catches all entities"));
	}

	/**
	 * normal rule followed by catch-all rule with * type. Should be all ok
	 */
	@Test
	public void testNormalAndCatchAllReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(2);
		launchRules.add(this.simpleRule1);
		launchRules.add(this.catchAllStarRule);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertTrue(warnings.isEmpty());
	}

	/**
	 * catch some and then all.
	 */
	@Test
	public void testCatchSomeAndAllReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(2);
		launchRules.add(this.catchSomeType1);
		launchRules.add(this.catchAllRule1);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertTrue(warnings.isEmpty());
	}

	/**
	 * catch all and then some of type (not ok).
	 */
	@Test
	public void testCatchAllAndSomeReachable() {
		final List<LaunchRule> launchRules = new ArrayList<>(2);
		launchRules.add(this.catchAllRule1);
		launchRules.add(this.catchSomeType1);
		final List<ValidatorWarning> warnings = MASValidator.checkLaunchRulesReachable(launchRules, null);
		assertFalse(warnings.isEmpty());
		assertTrue(warnings.get(0).toString().contains("unreachable because a previous rule catches all entities"));
	}

}
