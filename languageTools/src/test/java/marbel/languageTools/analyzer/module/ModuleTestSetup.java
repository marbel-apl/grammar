package marbel.languageTools.analyzer.module;

import java.util.ArrayList;
import java.util.List;

import marbel.languageTools.analyzer.FileRegistry;
import marbel.languageTools.analyzer.module.ModuleValidator;
import marbel.languageTools.errors.Message;
import marbel.languageTools.program.agent.Module;

public abstract class ModuleTestSetup {
	private final String path = "src/test/resources/marbel/languageTools/analyzer/module";

	private List<Message> syntaxerrors;
	private List<Message> errors;
	private List<Message> warnings;

	private Module program;

	/**
	 * Creates validator, calls validate, and initializes relevant fields.
	 *
	 * @param resource The module file used in the test.
	 */
	void setup(final String resource) {
		final FileRegistry registry = new FileRegistry();
		final ModuleValidator validator = new ModuleValidator(this.path + resource, registry);
		validator.validate();

		this.syntaxerrors = new ArrayList<>(registry.getSyntaxErrors());
		this.errors = new ArrayList<>(registry.getErrors());
		this.warnings = new ArrayList<>(registry.getWarnings());
		this.program = validator.getProgram();

		final List<Message> all = new ArrayList<>(this.syntaxerrors);
		all.addAll(this.errors);
		all.addAll(this.warnings);
		String source = "???";
		if (this.program != null && this.program.getSourceFile() != null) {
			source = this.program.getSourceFile().toString();
		}
		System.out.println(source + ": " + all);
	}

	List<Message> getSyntaxErrors() {
		return this.syntaxerrors;
	}

	List<Message> getErrors() {
		return this.errors;
	}

	List<Message> getWarnings() {
		return this.warnings;
	}

	String getKRInterface() {
		return this.program.getKRInterface().getClass().getName();
	}

	Module getProgram() {
		return this.program;
	}
}
